/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../scss/app.scss';


import $ from 'jquery';
import openMenu from './components/menu';
import flashMessage from './components/flash_message';
import initSummerNote from './components/summernote';
import initModal from './components/modal';
import initImages from './components/project-images';
import initSliderHome from './components/slider_home';

require('jquery-ui');
require('jquery-ui/ui/widgets/sortable');
require('jquery-ui/ui/disable-selection');

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');


$(function () {
 openMenu();
 flashMessage();
 initSummerNote();
 initModal();
 initImages();
 initSliderHome();
});