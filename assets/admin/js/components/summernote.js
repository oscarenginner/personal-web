import $ from 'jquery'
import './../vendors/summernote/summernote-lite.css';
import './../vendors/summernote/summernote-lite';

export default function() {
  const config = getConfiguration();

  $('.js-summernote').each(function() {
    $(this).summernote(config);
  })
}

function getConfiguration() {
  return {
    height: 250,
    emptyPara: '',
    toolbar: [
      ['style', ['bold', 'italic', 'clear']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['color', ['color']],
      ['picture'],
      ['video'],
      ['link'],
      ['hr'],
      ['misc', ['fullscreen', 'undo', 'redo']]
    ]
  }
}