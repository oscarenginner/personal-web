import $ from 'jquery'

export default function () {
    $('#js-open-menu').on('click',function () {
        $('body').toggleClass('open-sidebar');
        $('.js-menu-icon-close').toggleClass('hide');
        $('.js-menu-icon-open').toggleClass('hide');
    });
}