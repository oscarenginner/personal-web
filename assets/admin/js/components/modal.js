import $ from 'jquery'

export default function () {
    $('body').on('click','.js-open-modal',function (){
        let text = $(this).data('description');
        let src = $(this).data('src');
        let title = $(this).data('title');

        $('#js-modal-title').html(title);
        $('#js-modal-description').html(text);
        $('#js-modal-accept').attr('href',src);

        $('body').addClass('open-modal');
    });

    $('body').on('click','#js-modal-discard',function () {
        $('body').removeClass('open-modal');
    })

    let modal = $('.modal-advise');

    $(document).mouseup(function (e) {
        if (!modal.is(e.target) && modal.has(e.target).length === 0)
        {
            $('body').removeClass('open-modal');
        }
    });

}