export default function () {
        $( "#slider_home_list" ).sortable({
                update: function(event,ui) {
                        console.log(ui.item);
                        let id = ui.item.data('id');
                        let targetPosition = ui.item.index() + 1;

                        changePosition(id,targetPosition);
                }
        });
        $( "#slider_home_list" ).disableSelection();

}

function changePosition(id,targetPosition)
{
        let href = $('#slider_home_list').data('href');

        $.ajax(href, {
                method: 'GET',
                data: {id: id , targetPosition: targetPosition}
        }).done(function(response) {
                $( ".js-slider-home-position" ).each(function( i ) {
                       $(this).html(i + 1);
                });

        }).fail(function(jqXHR, textStatus) {

        })
}