export default function () {

    $('.js-media-delete').on('click',function(event){
        event.preventDefault();
        let image = $(this).data('image-id');
        let href = $(this).attr('href');

        deleteImage(image, href);
    });


}

function deleteImage($image,href)
{
    const route = '/ajax/image/project/remove';

    $.ajax(href, {
        method: 'GET',
        data: {image: $image}
    }).done(function(response) {

        $("#js-image-"+$image).fadeOut("slow" , function () {
            $("#js-image-"+$image).remove();
        });

    }).fail(function(jqXHR, textStatus) {
        console.error("Request failed: " + textStatus );
    })
}