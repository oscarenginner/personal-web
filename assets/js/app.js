/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../scss/app.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';
import 'slick-carousel';

import initMenu from './components/menu';



$(function () {
    $(".js-slider_skills").slick({
          autoplay: true
        , infinite: true
        , dots: false
        , slidesToShow: 8
        , slideswToScroll: 1
        , arrows: false,
        speed: 10000,
        autoplaySpeed: 0,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 768,
            settings: {
              slidesToShow: 2
            , slideswToScroll: 1,
            },

        },
            {
                breakpoint: 1024,
                settings: {
                      slidesToShow: 4
                    , slideswToScroll: 1
                },

            },
            {
                breakpoint: 1400,
                settings: {
                     slidesToShow: 6
                    , slideswToScroll: 1,
                },

            },
        ]


    });

    $('.js-projects-featured').slick({
        dots: true,
        arrows: false,
        rows: 0,
        speed: 2000,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 10000,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                dots: true,
                swipe: true,
                draggable: true,
                infinite: true,
                mobileFirst: true,
            }
        }]
    });

    initMenu();
});