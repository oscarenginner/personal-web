import $ from 'jquery'

export default function () {
    var open = false;

    $('.js-menu').click(function(){
        if (open == false){
            $('.menu').removeClass('close-menu');
            $('body').addClass('is-open-menu');
            $('.menu-navigation').removeClass('menu-close');
            $('.js-menu').addClass('open-close');
            $('.menu').addClass('open-menu');
            $('.menu-navigation').addClass('menu-open');
            open = true
        }
        else {
            $('.js-menu').removeClass('open-close');
            $('.menu').addClass('close-menu');
            $('body').removeClass('is-open-menu');
            $('.menu-navigation').addClass('menu-close');
            open = false
        }
    });
}