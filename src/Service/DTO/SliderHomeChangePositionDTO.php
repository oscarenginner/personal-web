<?php
/**
 * User: Oscar Sanchez
 * Date: 22/8/20
 */

namespace App\Service\DTO;


class SliderHomeChangePositionDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $targetPosition;

    /**
     * SliderHomeChangePositionDTO constructor.
     * @param int $id
     * @param int $targetPosition
     */
    public function __construct(int $id, int $targetPosition)
    {
        $this->id = $id;
        $this->targetPosition = $targetPosition;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTargetPosition(): int
    {
        return $this->targetPosition;
    }


}