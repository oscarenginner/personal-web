<?php
/**
 * User: Oscar Sanchez
 * Date: 20/8/20
 */

namespace App\Service\DTO;


class BlogCategoryDeleteDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $user;

    /**
     * @var int
     */
    private $id;

    /**
     * BlogCategoryDeleteDTO constructor.
     * @param int $user
     * @param int $id
     */
    public function __construct(int $user, int $id)
    {
        $this->user = $user;
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }




}