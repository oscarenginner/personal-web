<?php
/**
 * User: Oscar Sanchez
 * Date: 5/8/20
 */

namespace App\Service\DTO;


use App\Entity\Project;
use DateTime;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class ProjectUpdateDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $user;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $description;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $logo;

    /**
     * @var DateTime|null
     * @Assert\NotBlank()
     */
    private $date;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $imageFull;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $imageList;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $imageListPhone;

    /**
     * @var array
     */
    private $skills;

    /**
     * @var string
     * @Assert\Url
     */
    private $url;

    /**
     * @var File|null
     */
    private $file;

    /**
     * @var string
     */
    private $buttonFileText;

    /**
     * @var array
     */
    private $images;

    /**
     * @var string
     */
    private $shortDescription;

    /**
     * @var bool
     */
    private $featured;


    /**
     * ProjectCreateDTO constructor.
     * @param int $id
     * @param int $user
     * @param string $name
     * @param string $type
     * @param string $description
     * @param File|null $logo
     * @param DateTime|null $date
     * @param File|null $imageFull
     * @param string $url
     * @param File|null $file
     * @param string $buttonFileText
     * @param array $skills
     * @param string $shortDescription
     * @param bool $featured
     */
    private function __construct(int $id,
                                int $user,
                                string $name,
                                string $type,
                                string $description,
                                ?File $logo,
                                ?DateTime $date,
                                ?File $imageFull,
                                string $url,
                                ?File $file,
                                string  $buttonFileText,
                                 array $skills,
                                 string $shortDescription ,
                                 bool $featured)
    {
        $this->id = $id;
        $this->user = $user;
        $this->name = $name;
        $this->type = $type;
        $this->description = $description;
        $this->logo = $logo;
        $this->date = $date;
        $this->imageFull = $imageFull;
        $this->skills = $skills;
        $this->url = $url;
        $this->file = $file;
        $this->buttonFileText = $buttonFileText;
        $this->images = [];
        $this->shortDescription = $shortDescription;
        $this->featured = $featured;
    }


    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser(int $user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return File|null
     */
    public function getLogo(): ?File
    {
        return $this->logo;
    }

    /**
     * @param File|null $logo
     */
    public function setLogo(?File $logo): void
    {
        $this->logo = $logo;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime|null $date
     */
    public function setDate(?DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return File|null
     */
    public function getImageFull(): ?File
    {
        return $this->imageFull;
    }

    /**
     * @param File|null $imageFull
     */
    public function setImageFull(?File $imageFull): void
    {
        $this->imageFull = $imageFull;
    }

    /**
     * @return array
     */
    public function getSkills(): array
    {
        return $this->skills;
    }

    /**
     * @param array $skills
     */
    public function setSkills(array $skills): void
    {
        $this->skills = $skills;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $file
     */
    public function setFile(?File $file): void
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getButtonFileText(): string
    {
        return $this->buttonFileText;
    }

    /**
     * @param string $buttonFileText
     */
    public function setButtonFileText(string $buttonFileText): void
    {
        $this->buttonFileText = $buttonFileText;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param array $images
     */
    public function setImages(array $images): void
    {
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     */
    public function setShortDescription(string $shortDescription): void
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * @param Project $project
     * @param int $user
     * @return self
     */
    public static function createByProject(Project $project,int $user) :self
    {
        return new self(
            $project->getId(),
            $user,
            $project->getName(),
            $project->getType()->get(),
            $project->getDescription(),
            null,
            $project->getDate(),
            null,
            $project->getUrl(),
            null,
            ($project->getButtonFileText()) ? $project->getButtonFileText() : '',
            $project->getSkillToPairValuesArray(),
            ($project->getShortDescription()) ? $project->getShortDescription() : '',
            ($project->isFeatured()) ? $project->isFeatured() : false
        );
    }

    /**
     * @return File|null
     */
    public function getImageList(): ?File
    {
        return $this->imageList;
    }

    /**
     * @param File|null $imageList
     */
    public function setImageList(?File $imageList): void
    {
        $this->imageList = $imageList;
    }

    /**
     * @return File|null
     */
    public function getImageListPhone(): ?File
    {
        return $this->imageListPhone;
    }

    /**
     * @param File|null $imageListPhone
     */
    public function setImageListPhone(?File $imageListPhone): void
    {
        $this->imageListPhone = $imageListPhone;
    }

    /**
     * @return bool
     */
    public function isFeatured(): bool
    {
        return $this->featured;
    }

    /**
     * @param bool $featured
     */
    public function setFeatured(bool $featured): void
    {
        $this->featured = $featured;
    }


}