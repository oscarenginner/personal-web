<?php
/**
 * User: Oscar Sanchez
 * Date: 31/7/20
 */

namespace App\Service\DTO;


class SkIllCategoryDeleteDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * SkIllCategoryDeleteDTO constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


}