<?php
/**
 * User: Oscar Sanchez
 * Date: 4/8/20
 */

namespace App\Service\DTO;


class ExperienceDeleteDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $user;

    /**
     * ExperienceDeleteDTO constructor.
     * @param int $id
     * @param int $user
     */
    public function __construct(int $id, int $user)
    {
        $this->id = $id;
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser(int $user): void
    {
        $this->user = $user;
    }




}