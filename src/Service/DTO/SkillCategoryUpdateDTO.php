<?php
/**
 * User: Oscar Sanchez
 * Date: 30/7/20
 */

namespace App\Service\DTO;


use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AppAssert;

class SkillCategoryUpdateDTO implements DTOInterface
{

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     * @Assert\NotBlank()
     * @AppAssert\SkillCategoryInSystemConstraint(update = true)
     */
    private $name;

    /**
     * @var File
     */
    private $icon;

    /**
     * SkillCategoryUpdateDTO constructor.
     * @param int $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return File
     */
    public function getIcon(): ?File
    {
        return $this->icon;
    }

    /**
     * @param File $icon
     */
    public function setIcon(File $icon): void
    {
        $this->icon = $icon;
    }


}