<?php
/**
 * User: Oscar Sanchez
 * Date: 28/12/20
 */

namespace App\Service\DTO;


class BlogQueryDTO implements DTOInterface
{
    const DEFAULT_LIMIT = 4;
    /**
     * @var PagedQueryDTO
     */
    private $pagedQuery;

    /**
     * BlogQueryDTO constructor.
     * @param PagedQueryDTO $pagedQuery
     */
    public function __construct(PagedQueryDTO $pagedQuery)
    {
        $this->pagedQuery = $pagedQuery;
    }


    /**
     * @return PagedQueryDTO
     */
    public function getPagedQuery(): PagedQueryDTO
    {
        return $this->pagedQuery;
    }

    /**
     * @param PagedQueryDTO $pagedQuery
     */
    public function setPagedQuery(PagedQueryDTO $pagedQuery): void
    {
        $this->pagedQuery = $pagedQuery;
    }

    /**
     * @param array $query
     * @return BlogQueryDTO
     */
    public static function createFromQuery(array $query): BlogQueryDTO
    {
        $pagedQuery = new PagedQueryDTO(
            isset($query['page']) ? $query['page'] : 1,
            isset($query['limit']) ? $query['limit'] : self::DEFAULT_LIMIT
        );

        return new self($pagedQuery);
    }


}