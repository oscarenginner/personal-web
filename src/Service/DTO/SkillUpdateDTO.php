<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Service\DTO;


use App\Entity\Skill;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AppAssert;

class SkillUpdateDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @AppAssert\SkillInSystemConstraint(update = true)
     */
    private $name;

    /**
     * @var int
     * @Assert\Range(
     *      min = 0,
     *      max = 100,
     *      notInRangeMessage = "You must be between {{ min }}cm and {{ max }}",
     * )
     */
    private $score;

    /**
     * @var int
     */
    private $skillCategory;

    /**
     * @var int
     */
    private $user;

    /**
     * @var bool
     */
    private $featured;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $image;

    /**
     * SkillUpdateDTO constructor.
     * @param int $id
     * @param string $name
     * @param int $score
     * @param int $skillCategory
     * @param int $user
     * @param bool $featured
     */
    private function __construct(int $id, string $name, int $score, int $skillCategory, int $user, bool $featured)
    {
        $this->id = $id;
        $this->name = $name;
        $this->score = $score;
        $this->skillCategory = $skillCategory;
        $this->user = $user;
        $this->featured = $featured;
        $this->image = null;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function setScore(int $score): void
    {
        $this->score = $score;
    }

    /**
     * @return int
     */
    public function getSkillCategory(): int
    {
        return $this->skillCategory;
    }

    /**
     * @param int $skillCategory
     */
    public function setSkillCategory(int $skillCategory): void
    {
        $this->skillCategory = $skillCategory;
    }

    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser(int $user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }


    /**
     * @param Skill $skill
     * @return SkillUpdateDTO
     */
    public static function createFormSkill(Skill $skill)
    {
        return new self(
            $skill->getId(),
            $skill->getName(),
            $skill->getScore(),
            $skill->getSkillCategory()->getId(),
            $skill->getUser()->getId(),
        ($skill->isFeatured()) ? $skill->isFeatured() : 0);
    }

    /**
     * @return bool
     */
    public function isFeatured(): bool
    {
        return $this->featured;
    }

    /**
     * @param bool $featured
     */
    public function setFeatured(bool $featured): void
    {
        $this->featured = $featured;
    }

    /**
     * @return File|null
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

}