<?php
/**
 * User: Oscar Sanchez
 * Date: 19/8/20
 */

namespace App\Service\DTO;


class ProjectImageDeleteDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $imageId;

    /**
     * ProjectImageDeleteDTO constructor.
     * @param int $imageId
     */
    public function __construct(int $imageId)
    {
        $this->imageId = $imageId;
    }


    /**
     * @return int
     */
    public function getImageId(): int
    {
        return $this->imageId;
    }
}