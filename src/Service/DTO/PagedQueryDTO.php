<?php

namespace App\Service\DTO;

class PagedQueryDTO implements DTOInterface
{
    const MAX_PAGINATION = 120;
    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $limit;

    /**
     * PagedQueryDTO constructor.
     *
     * @param int $page
     * @param int $limit
     */
    public function __construct(int $page, int $limit)
    {
        $this->page = $page;
        $this->setLimit($limit);
    }

    /**
     * @return int
     */
    public function page(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function offset(): int
    {
        return $this->page() > 1 ? ($this->page() - 1) * $this->limit() : 0;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }

    /**
     * @param $limit
     */
    private function setLimit($limit): void
    {
        $this->limit = ($limit < self::MAX_PAGINATION)
            ? $limit
            : self::MAX_PAGINATION;
    }
}
