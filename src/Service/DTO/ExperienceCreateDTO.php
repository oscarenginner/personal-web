<?php
/**
 * User: Oscar Sanchez
 * Date: 4/8/20
 */

namespace App\Service\DTO;


use App\Entity\User;
use DateTime;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class ExperienceCreateDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $user;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $company;

    /**
     * @var DateTime|null
     */
    private $initDate;

    /**
     * @var DateTime|null
     */
    private $endDate;

    /**
     * @var File|null
     */
    private $image;

    /**
     * @var string
     */
    private $jobPosition;

    /**
     * @var string
     */
    private $jobDescription;

    /**
     * @var array
     */
    private $skills;

    /**
     * ExperienceCreateDTO constructor.
     * @param int $user
     * @param string $company
     * @param DateTime|null $initDate
     * @param DateTime|null $endDate
     * @param string|null $image
     * @param string $jobPosition
     * @param string $jobDescription
     */
    public function __construct(int $user,
                                string $company = '',
                                ?DateTime $initDate = null,
                                ?DateTime $endDate = null,
                                string $image = null,
                                string $jobPosition = '',
                                string $jobDescription='')
    {
        $this->user = $user;
        $this->company = $company;
        $this->initDate = $initDate;
        $this->endDate = $endDate;
        $this->image = $image;
        $this->jobPosition = $jobPosition;
        $this->jobDescription = $jobDescription;
        $this->skills = [];
    }

    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @return DateTime|null
     */
    public function getInitDate(): ?DateTime
    {
        return $this->initDate;
    }


    /**
     * @return DateTime|null
     */
    public function getEndDate(): ?DateTime
    {
        return $this->endDate;
    }

    /**
     * @return File|null
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getJobPosition(): string
    {
        return $this->jobPosition;
    }

    /**
     * @return string
     */
    public function getJobDescription(): string
    {
        return $this->jobDescription;
    }

    /**
     * @param int $user
     */
    public function setUser(int $user): void
    {
        $this->user = $user;
    }

    /**
     * @param string $company
     */
    public function setCompany(string $company): void
    {
        $this->company = $company;
    }

    /**
     * @param DateTime|null $initDate
     */
    public function setInitDate(?DateTime $initDate): void
    {
        $this->initDate = $initDate;
    }

    /**
     * @param DateTime|null $endDate
     */
    public function setEndDate(?DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

    /**
     * @param string $jobPosition
     */
    public function setJobPosition(string $jobPosition): void
    {
        $this->jobPosition = $jobPosition;
    }

    /**
     * @param string $jobDescription
     */
    public function setJobDescription(string $jobDescription): void
    {
        $this->jobDescription = $jobDescription;
    }

    /**
     * @return array
     */
    public function getSkills(): array
    {
        return $this->skills;
    }

    /**
     * @param array $skills
     */
    public function setSkills(array $skills): void
    {
        $this->skills = $skills;
    }


}