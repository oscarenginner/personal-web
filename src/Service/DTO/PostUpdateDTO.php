<?php
/**
 * User: Oscar Sanchez
 * Date: 20/8/20
 */

namespace App\Service\DTO;


use App\Entity\Post;
use App\Entity\User;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class PostUpdateDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     */
    private $subtitle;

    /**
     * @var string
     */
    private $content;

    /**
     * @var bool
     */
    private $publish;

    /**
     * @var int
     */
    private $author;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $image;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $imageList;

    /**
     * @var int
     */
    private $category;

    /**
     * @var string
     */
    private $excerpt;

    /**
     * @var string
     */
    private $metaTitle;

    /**
     * @var string
     */
    private $metaDescription;

    /**
     * PostUpdateDTO constructor.
     * @param string $title
     * @param string $subtitle
     * @param string $content
     * @param bool $publish
     * @param int $author
     * @param int $category
     * @param string $excerpt
     * @param string $metaTitle
     * @param string $metaDescription
     */
    public function __construct(int $id,
                                string $title,
                                string $subtitle,
                                string $content,
                                bool $publish,
                                int $author,
                                int $category,
                                string $excerpt,
                                string $metaTitle,
                                string $metaDescription)
    {
        $this->id = $id;
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->content = $content;
        $this->publish = $publish;
        $this->author = $author;
        $this->category = $category;
        $this->excerpt = $excerpt;
        $this->metaTitle = $metaTitle;
        $this->metaDescription = $metaDescription;
    }


    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return bool
     */
    public function isPublish(): bool
    {
        return $this->publish;
    }

    /**
     * @param bool $publish
     */
    public function setPublish(bool $publish): void
    {
        $this->publish = $publish;
    }

    /**
     * @return int
     */
    public function getAuthor(): int
    {
        return $this->author;
    }

    /**
     * @param int $author
     */
    public function setAuthor(int $author): void
    {
        $this->author = $author;
    }

    /**
     * @return File|null
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @param File $image
     */
    public function setImage(File $image): void
    {
        $this->image = $image;
    }

    /**
     * @return File|null
     */
    public function getImageList(): ?File
    {
        return $this->imageList;
    }

    /**
     * @param File $imageList
     */
    public function setImageList(File $imageList): void
    {
        $this->imageList = $imageList;
    }

    /**
     * @return int
     */
    public function getCategory(): int
    {
        return $this->category;
    }

    /**
     * @param int $category
     */
    public function setCategory(int $category): void
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getExcerpt(): string
    {
        return $this->excerpt;
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt(string $excerpt): void
    {
        $this->excerpt = $excerpt;
    }

    /**
     * @return string
     */
    public function getMetaTitle(): string
    {
        return $this->metaTitle;
    }

    /**
     * @param string $metaTitle
     */
    public function setMetaTitle(string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription(string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public static function createByPostAndUserId(Post $post, int $userId)
    {
        return new self(
            $post->getId(),
            ($post->getTitle()) ? $post->getTitle() : '',
            ($post->getSubtitle()) ? $post->getSubtitle() : '',
            ($post->getContent()) ? $post->getContent() : '',
            $post->isPublished(),
            $userId,
            $post->getCategory()->getId(),
            ($post->getExcerpt()) ? $post->getExcerpt() : '',
            ($post->getMetaTitle()) ? $post->getMetaTitle() : '',
            ($post->getMetaDescription()) ? $post->getMetaDescription() : ''
        );
    }


}