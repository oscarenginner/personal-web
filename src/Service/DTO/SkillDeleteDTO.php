<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Service\DTO;


class SkillDeleteDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * SkillDeleteDTO constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}