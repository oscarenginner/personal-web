<?php
/**
 * User: Oscar Sanchez
 * Date: 6/8/20
 */

namespace App\Service\DTO;


class ProjectDeleteDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $user;

    /**
     * ProjectDeleteDTO constructor.
     * @param int $id
     * @param int $user
     */
    public function __construct(int $id, int $user)
    {
        $this->id = $id;
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }



}