<?php
/**
 * User: Oscar Sanchez
 * Date: 20/8/20
 */

namespace App\Service\DTO;


use App\Entity\BlogCategory;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class BlogCategoryUpdateDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     */
    private $subtitle;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $metaTitle;

    /**
     * @var string
     */
    private $metaDescription;

    /**
     * @var int
     */
    private $user;

    /**
     * @var File|null
     */
    private $image;

    /**
     * BlogCategoryUpdateDTO constructor.
     * @param int $id
     * @param string $title
     * @param string $subtitle
     * @param string $content
     * @param string $metaTitle
     * @param string $metaDescription
     * @param int $user
     * @param File|null $image
     */
    public function __construct(int $id,
                                string $title,
                                string $subtitle,
                                string $content,
                                string $metaTitle,
                                string $metaDescription,
                                int $user,
                                ?File $image)
    {
        $this->id = $id;
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->content = $content;
        $this->metaTitle = $metaTitle;
        $this->metaDescription = $metaDescription;
        $this->user = $user;
        $this->image = $image;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getMetaTitle(): string
    {
        return $this->metaTitle;
    }

    /**
     * @param string $metaTitle
     */
    public function setMetaTitle(string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription(string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser(int $user): void
    {
        $this->user = $user;
    }

    /**
     * @return File|null
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

    /**
     * @param BlogCategory $blogCategory
     * @param int $userId
     * @return $this
     */
    public static function createByCategoryAndUserId(BlogCategory $blogCategory, int $userId) : self
    {
        return new self(
            $blogCategory->getId(),
            ($blogCategory->getTitle()) ? $blogCategory->getTitle() : '',
            ($blogCategory->getSubtitle()) ? $blogCategory->getSubtitle() : '',
            ($blogCategory->getContent()) ? $blogCategory->getContent() : '',
            ($blogCategory->getMetaTitle()) ? $blogCategory->getMetaDescription() : '',
            ($blogCategory->getMetaDescription()) ? $blogCategory->getMetaDescription() : '',
            $userId,
            null
        );
    }


}