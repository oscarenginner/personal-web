<?php
/**
 * User: Oscar Sanchez
 * Date: 28/7/20
 */

namespace App\Service\DTO;


use App\Entity\User;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class UserUpdateDTO implements DTOInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $phone;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $surname;

    /**
     * @Assert\Url
     * @var string
     */
    private $instagramLink;

    /**
     * @Assert\Url
     * @var string
     */
    private $facebookLink;

    /**
     * @Assert\Url
     * @var string
     */
    private $githubLink;

    /**
     * @Assert\Url
     * @var string
     */
    private $hackerRankLink;

    /**
     * @Assert\Url
     * @var string
     */
    private $linkedinLink;

    /**
     * @Assert\Url
     * @var string
     */
    private $codepenLink;

    /**
     * @Assert\Url
     * @var string
     */
    private $whatsappLink;

    /**
     * @var string
     */
    private $shortDescription;

    /**
     * @var string
     */
    private $history;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $avatar;


    /**
     * UserUpdateDTO constructor.
     * @param int $id
     * @param string $phone
     * @param string $name
     * @param string $surname
     * @param string $instagramLink
     * @param string $facebookLink
     * @param string $githubLink
     * @param string $shortDescription
     * @param string $history
     * @param File|null $file
     * @param string $hackerRankLink
     * @param string $linkedinLink
     * @param string $codepenLink
     * @param string $whatsappLink
     */
    public function __construct(int $id,
                                string $phone,
                                string $name, string
                                $surname, string
                                $instagramLink,
                                string $facebookLink,
                                string $githubLink,
                                string $shortDescription,
                                string $history,
                                ?File $file,
                                string $hackerRankLink,
                                string $linkedinLink,
                                string $codepenLink,
                                string $whatsappLink
                                )
    {
        $this->id = $id;
        $this->phone = $phone;
        $this->name = $name;
        $this->surname = $surname;
        $this->instagramLink = $instagramLink;
        $this->facebookLink = $facebookLink;
        $this->githubLink = $githubLink;
        $this->shortDescription = $shortDescription;
        $this->history = $history;
        $this->avatar = $file;
        $this->hackerRankLink = $hackerRankLink;
        $this->linkedinLink = $linkedinLink;
        $this->codepenLink = $codepenLink;
        $this->whatsappLink = $whatsappLink;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getInstagramLink(): string
    {
        return $this->instagramLink;
    }

    /**
     * @param string $instagramLink
     */
    public function setInstagramLink(string $instagramLink): void
    {
        $this->instagramLink = $instagramLink;
    }

    /**
     * @return string
     */
    public function getFacebookLink(): string
    {
        return $this->facebookLink;
    }

    /**
     * @param string $facebookLink
     */
    public function setFacebookLink(string $facebookLink): void
    {
        $this->facebookLink = $facebookLink;
    }

    /**
     * @return string
     */
    public function getGithubLink(): string
    {
        return $this->githubLink;
    }

    /**
     * @param string $githubLink
     */
    public function setGithubLink(string $githubLink): void
    {
        $this->githubLink = $githubLink;
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @param User $user
     * @return UserUpdateDTO
     */
    public static function createByUser(User $user)
    {
        return new self(
            $user->getId(),
            $user->getPhone(),
            ($user->getName()) ? $user->getName() : ' ',
            $user->getSurname(),
            $user->getInstagramLink(),
            $user->getFacebookLink(),
            $user->getGithubLink(),
            $user->getShortDescription(),
            ($user->getHistory()) ? $user->getHistory() : ' ',
            null,
            ($user->getHackerRankLink()) ? $user->getHackerRankLink() : ' ',
            ($user->getLinkedinLink()) ? $user->getLinkedinLink() : ' ',
            ($user->getCodepenLink()) ? $user->getCodepenLink() : ' ',
            ($user->getWhatsappLink()) ? $user->getWhatsappLink() : ' '

        );
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     */
    public function setShortDescription(string $shortDescription): void
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * @return string
     */
    public function getHistory(): string
    {
        return $this->history;
    }

    /**
     * @param string $history
     */
    public function setHistory(string $history): void
    {
        $this->history = $history;
    }

    /**
     * @return File|null
     */
    public function getAvatar(): ?File
    {
        return $this->avatar;
    }

    /**
     * @param File|null $avatar
     */
    public function setAvatar(?File $avatar): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @return string
     */
    public function getHackerRankLink(): string
    {
        return $this->hackerRankLink;
    }

    /**
     * @param string $hackerRankLink
     */
    public function setHackerRankLink(string $hackerRankLink): void
    {
        $this->hackerRankLink = $hackerRankLink;
    }

    /**
     * @return string
     */
    public function getLinkedinLink(): string
    {
        return $this->linkedinLink;
    }

    /**
     * @param string $linkedinLink
     */
    public function setLinkedinLink(string $linkedinLink): void
    {
        $this->linkedinLink = $linkedinLink;
    }

    /**
     * @return string
     */
    public function getCodepenLink(): string
    {
        return $this->codepenLink;
    }

    /**
     * @param string $codepenLink
     */
    public function setCodepenLink(string $codepenLink): void
    {
        $this->codepenLink = $codepenLink;
    }

    /**
     * @return string
     */
    public function getWhatsappLink(): string
    {
        return $this->whatsappLink;
    }

    /**
     * @param string $whatsappLink
     */
    public function setWhatsappLink(string $whatsappLink): void
    {
        $this->whatsappLink = $whatsappLink;
    }
}