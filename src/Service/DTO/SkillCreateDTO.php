<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Service\DTO;


use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AppAssert;

class SkillCreateDTO implements DTOInterface
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @AppAssert\SkillInSystemConstraint()
     */
    private $name;

    /**
     * @var int
     * @Assert\Range(
     *      min = 0,
     *      max = 100,
     *      notInRangeMessage = "You must be between {{ min }}cm and {{ max }}",
     * )
     */
    private $score;

    /**
     * @var int
     */
    private $skillCategory;

    /**
     * @var int
     */
    private $user;

    /**
     * @var bool
     */
    private $featured;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $image;


    /**
     * CreateSkillDTO constructor.
     * @param int $user
     * @param string $name
     * @param int $score
     * @param int $skillCategory
     */
    public function __construct(int $user, string $name = '', int $score = 0, int $skillCategory = 0)
    {
        $this->user = $user;
        $this->name = $name;
        $this->score = $score;
        $this->skillCategory = $skillCategory;
        $this->featured = false;
        $this->image = null;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function setScore(int $score): void
    {
        $this->score = $score;
    }

    /**
     * @return int
     */
    public function getSkillCategory(): int
    {
        return $this->skillCategory;
    }

    /**
     * @param int $skillCategory
     */
    public function setSkillCategory(int $skillCategory): void
    {
        $this->skillCategory = $skillCategory;
    }

    /**
     * @return int
     */
    public function getUser(): int
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function isFeatured(): bool
    {
        return $this->featured;
    }

    /**
     * @param bool $featured
     */
    public function setFeatured(bool $featured): void
    {
        $this->featured = $featured;
    }

    /**
     * @return File|null
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }


}