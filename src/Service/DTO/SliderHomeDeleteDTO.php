<?php
/**
 * User: Oscar Sanchez
 * Date: 21/8/20
 */

namespace App\Service\DTO;


class SliderHomeDeleteDTO implements DTOInterface
{
    private $id;

    /**
     * SliderHomeDeleteDTO constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }




}