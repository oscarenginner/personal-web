<?php
/**
 * User: Oscar Sanchez
 * Date: 21/8/20
 */

namespace App\Service\DTO;


use App\Entity\SliderHome;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class SliderHomeUpdateDTO implements DTOInterface
{

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     */
    private $subtitle;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $image;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $imagePhone;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $videoYoutubeLink;

    /**
     * @var string
     */
    private $buttonText;

    /**
     * SliderCreateDTO constructor.
     * @param int $id
     * @param string $title
     * @param string $subtitle
     * @param string $link
     * @param string $buttonText
     * @param string $videoYoutubeLink
     */
    public function __construct(int $id ,string $title,string $subtitle,string $link,string $buttonText,string $videoYoutubeLink)
    {
        $this->id = $id;
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->link = $link;
        $this->buttonText = $buttonText;
        $this->videoYoutubeLink = $videoYoutubeLink;

    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return File|null
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

    /**
     * @return File|null
     */
    public function getImagePhone(): ?File
    {
        return $this->imagePhone;
    }

    /**
     * @param File|null $imagePhone
     */
    public function setImagePhone(?File $imagePhone): void
    {
        $this->imagePhone = $imagePhone;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getVideoYoutubeLink(): string
    {
        return $this->videoYoutubeLink;
    }

    /**
     * @param string $videoYoutubeLink
     */
    public function setVideoYoutubeLink(string $videoYoutubeLink): void
    {
        $this->videoYoutubeLink = $videoYoutubeLink;
    }

    /**
     * @return string
     */
    public function getButtonText(): string
    {
        return $this->buttonText;
    }

    /**
     * @param string $buttonText
     */
    public function setButtonText(string $buttonText): void
    {
        $this->buttonText = $buttonText;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public static function createBySliderHome(SliderHome $sliderHome) : self
    {
        return new self(
            $sliderHome->getId(),
            ($sliderHome->getTitle()) ? $sliderHome->getTitle() : '',
            ($sliderHome->getSubtitle()) ? $sliderHome->getSubtitle() : '',
            ($sliderHome->getLink()) ? $sliderHome->getLink() : '',
            ($sliderHome->getTextButton()) ? $sliderHome->getTextButton() : '',
            ($sliderHome->getVideoYoutubeLink()) ? $sliderHome->getVideoYoutubeLink() : ''
        );
    }


}