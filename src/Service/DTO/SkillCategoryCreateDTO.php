<?php
/**
 * User: Oscar Sanchez
 * Date: 30/7/20
 */

namespace App\Service\DTO;


use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AppAssert;

class SkillCategoryCreateDTO implements DTOInterface
{

    /**
     * @var string
     * @Assert\NotBlank()
     * @AppAssert\SkillCategoryInSystemConstraint()
     */
    private $name;

    /**
     * @var File
     * @Assert\File(
     *     mimeTypes = {"image/svg+xml","application/svg+xml"},
     *     mimeTypesMessage = "Please upload a valid SVG")
     */
    private $icon;
    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return File
     */
    public function getIcon(): ?File
    {
        return $this->icon;
    }

    /**
     * @param File $icon
     */
    public function setIcon(File $icon): void
    {
        $this->icon = $icon;
    }


}