<?php
/**
 * User: Oscar Sanchez
 * Date: 21/8/20
 */

namespace App\Service\DTO;


use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class SliderHomeCreateDTO implements DTOInterface
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     */
    private $subtitle;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $image;

    /**
     * @var File|null
     * @Assert\Image
     */
    private $imagePhone;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $videoYoutubeLink;

    /**
     * @var string
     */
    private $buttonText;

    /**
     * SliderCreateDTO constructor.
     */
    public function __construct()
    {
        $this->title = '';
        $this->subtitle = '';
        $this->link = '';
        $this->buttonText = '';
        $this->videoYoutubeLink = '';

    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return File|null
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

    /**
     * @return File|null
     */
    public function getImagePhone(): ?File
    {
        return $this->imagePhone;
    }

    /**
     * @param File|null $imagePhone
     */
    public function setImagePhone(?File $imagePhone): void
    {
        $this->imagePhone = $imagePhone;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getVideoYoutubeLink(): string
    {
        return $this->videoYoutubeLink;
    }

    /**
     * @param string $videoYoutubeLink
     */
    public function setVideoYoutubeLink(string $videoYoutubeLink): void
    {
        $this->videoYoutubeLink = $videoYoutubeLink;
    }

    /**
     * @return string
     */
    public function getButtonText(): string
    {
        return $this->buttonText;
    }

    /**
     * @param string $buttonText
     */
    public function setButtonText(string $buttonText): void
    {
        $this->buttonText = $buttonText;
    }


}