<?php
/**
 * User: Oscar Sanchez
 * Date: 2/8/20
 */

namespace App\Service\Handler;


use App\Repository\SkillCategoryRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\SkillCategoryUpdateDTO;

class SkillCategoryUpdateHandler implements ServiceHandlerInterface
{
    /**
     * @var SkillCategoryRepositoryInterface
     */
    private $skillCategoryRepository;

    /**
     * SkillCategoryUpdateHandler constructor.
     * @param SkillCategoryRepositoryInterface $skillCategoryRepository
     */
    public function __construct(SkillCategoryRepositoryInterface $skillCategoryRepository)
    {
        $this->skillCategoryRepository = $skillCategoryRepository;
    }


    /**
     * @param DTOInterface|SkillCategoryUpdateDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $skillCategory = $this->skillCategoryRepository->getById($dto->getId());

        if($dto->getIcon()) {
            $skillCategory->deleteIcon();
            $skillCategory->addIcon($dto->getIcon());

        }

        $skillCategory->setName($dto->getName());
        $this->skillCategoryRepository->save($skillCategory);

    }
}