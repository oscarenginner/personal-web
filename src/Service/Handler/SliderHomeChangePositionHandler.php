<?php
/**
 * User: Oscar Sanchez
 * Date: 22/8/20
 */

namespace App\Service\Handler;


use App\Entity\SliderHome;
use App\Repository\SliderHomeRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\SliderHomeChangePositionDTO;

class SliderHomeChangePositionHandler implements ServiceHandlerInterface
{
    /**
     * @var SliderHomeRepositoryInterface
     */
    private $sliderHomeRepository;

    /**
     * SliderHomeChangePositionHandler constructor.
     * @param SliderHomeRepositoryInterface $sliderHomeRepository
     */
    public function __construct(SliderHomeRepositoryInterface $sliderHomeRepository)
    {
        $this->sliderHomeRepository = $sliderHomeRepository;
    }


    /**
     * @param DTOInterface|SliderHomeChangePositionDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
       $sliderHome = $this->sliderHomeRepository->getById($dto->getId());

       $position = $sliderHome->getPosition();

       if($position > $dto->getTargetPosition()) {

           $sliders = $this->sliderHomeRepository->getEqualsAndUpPositionRangeByTwoPositions($dto->getTargetPosition(),$position);

           /** @var SliderHome $slider */
           foreach ($sliders as $slider) {
               $newPosition = $slider->getPosition() + 1;
               $slider->setPosition($newPosition);
               $this->sliderHomeRepository->save($slider);
           }


           $sliderHome->setPosition($dto->getTargetPosition());
           $this->sliderHomeRepository->save($sliderHome);

       }

       if($position < $dto->getTargetPosition() ) {

           $sliders = $this->sliderHomeRepository->getDownPositionRangeByTwoPositions($dto->getTargetPosition(),$position);

           /** @var SliderHome $slider */
           foreach ($sliders as $slider) {
               $newPosition = $slider->getPosition() - 1;
               $slider->setPosition($newPosition);
               $this->sliderHomeRepository->save($slider);
           }

           $sliderHome->setPosition($dto->getTargetPosition());
           $this->sliderHomeRepository->save($sliderHome);
       }
    }
}