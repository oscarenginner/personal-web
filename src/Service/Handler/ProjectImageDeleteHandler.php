<?php
/**
 * User: Oscar Sanchez
 * Date: 19/8/20
 */

namespace App\Service\Handler;


use App\Repository\ProjectRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\ProjectImageDeleteDTO;

class ProjectImageDeleteHandler implements ServiceHandlerInterface
{
    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepository;

    /**
     * ProjectImageDeleteHandler constructor.
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }


    /**
     * @param DTOInterface|ProjectImageDeleteDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $project = $this->projectRepository->getByImageId($dto->getImageId());

        $project->removeImage($dto->getImageId());
        $tet = $project->getImages();
        $this->projectRepository->save($project);
        $tst = $project->getImages();

    }
}