<?php
/**
 * User: Oscar Sanchez
 * Date: 21/8/20
 */

namespace App\Service\Handler;


use App\Exception\UserNotIsPropertyOfObjectException;
use App\Repository\BlogCategoryRepositoryInterface;
use App\Repository\PostRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\PostUpdateDTO;

class PostUpdateHandler implements ServiceHandlerInterface
{
    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * @var BlogCategoryRepositoryInterface
     */
    private $blogCategoryRepository;

    /**
     * PostUpdateHandler constructor.
     * @param PostRepositoryInterface $postRepository
     * @param BlogCategoryRepositoryInterface $blogCategoryRepository
     */
    public function __construct(PostRepositoryInterface $postRepository,
                                BlogCategoryRepositoryInterface $blogCategoryRepository)
    {
        $this->postRepository = $postRepository;
        $this->blogCategoryRepository = $blogCategoryRepository;
    }


    /**
     * @param DTOInterface|PostUpdateDTO $dto
     * @return mixed|void
     * @throws UserNotIsPropertyOfObjectException
     */
    public function handle(DTOInterface $dto)
    {
        $post = $this->postRepository->getById($dto->getId());

        if($post->getAuthor()->getId() != $post->getAuthor()->getId()) {
            throw new UserNotIsPropertyOfObjectException();
        }

        $post->setTitle($dto->getTitle());
        $post->setSubtitle($dto->getSubtitle());
        $post->setContent($dto->getContent());
        $post->setMetaTitle($dto->getMetaTitle());
        $post->setMetaDescription($dto->getMetaDescription());
        $post->setExcerpt($dto->getExcerpt());

        if($dto->isPublish() != $post->isPublished()) {
            if($dto->isPublish()) {
                $post->publish();
            }else{
                $post->unPublish();
            }
        }

        if($dto->getImage()) {
            $post->addImage($dto->getImage());
        }

        if($dto->getImageList()) {
            $post->addImageList($dto->getImageList());
        }

        if($post->getCategory()->getId() != $dto->getCategory()) {

            $blogCategory = $this->blogCategoryRepository->getById($dto->getId());

            $post->setCategory($blogCategory);
        }

        $this->postRepository->save($post);
    }

}