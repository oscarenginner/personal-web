<?php
/**
 * User: Oscar Sanchez
 * Date: 20/8/20
 */

namespace App\Service\Handler;


use App\Exception\UserNotIsPropertyOfObjectException;
use App\Repository\BlogCategoryRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Service\DTO\BlogCategoryUpdateDTO;
use App\Service\DTO\DTOInterface;

class BlogCategoryUpdateHandler implements ServiceHandlerInterface
{
    /**
     * @var BlogCategoryRepositoryInterface
     */
    private $blogCategoryRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * BlogCategoryUpdateHandler constructor.
     * @param BlogCategoryRepositoryInterface $blogCategoryRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(BlogCategoryRepositoryInterface $blogCategoryRepository,
                                UserRepositoryInterface $userRepository)
    {
        $this->blogCategoryRepository = $blogCategoryRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param DTOInterface|BlogCategoryUpdateDTO $dto
     * @return mixed|void
     * @throws UserNotIsPropertyOfObjectException
     */
    public function handle(DTOInterface $dto)
    {
        $blogCategory = $this->blogCategoryRepository->getById($dto->getId());

        if($blogCategory->getUser()->getId() != $dto->getUser()) {
            throw new UserNotIsPropertyOfObjectException();
        }

        $blogCategory->setTitle($dto->getTitle());
        $blogCategory->setSubtitle($dto->getSubtitle());
        $blogCategory->setMetaTitle($dto->getMetaTitle());
        $blogCategory->setMetaDescription($dto->getMetaDescription());

        if($dto->getImage()) {
            $blogCategory->addImage($dto->getImage());
        }

        $this->blogCategoryRepository->save($blogCategory);
    }
}