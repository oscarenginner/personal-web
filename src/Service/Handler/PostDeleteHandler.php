<?php
/**
 * User: Oscar Sanchez
 * Date: 20/8/20
 */

namespace App\Service\Handler;


use App\Exception\UserNotIsPropertyOfObjectException;
use App\Repository\PostRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\PostDeleteDTO;

class PostDeleteHandler implements ServiceHandlerInterface
{
    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * PostDeleteHandler constructor.
     * @param PostRepositoryInterface $postRepository
     */
    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }


    /**
     * @param DTOInterface|PostDeleteDTO $dto
     * @return mixed|void
     * @throws UserNotIsPropertyOfObjectException
     */
    public function handle(DTOInterface $dto)
    {
        $post = $this->postRepository->getById($dto->getId());

        if($dto->getUser() != $post->getAuthor()->getId()) {
            throw new UserNotIsPropertyOfObjectException();
        }

        $post->deleteMedias();

        $this->postRepository->delete($post);


    }
}