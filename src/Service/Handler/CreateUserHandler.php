<?php

namespace App\Service\Handler;

use App\Entity\User;
use App\Entity\ValueObject\PasswordEncoded;
use App\Repository\UserRepositoryInterface;
use App\Service\DTO\CreateUserDTO;
use App\Service\DTO\DTOInterface;
use App\Service\Handler\ServiceHandlerInterface;

class CreateUserHandler implements ServiceHandlerInterface
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    /**
     * @param DTOInterface|CreateUserDTO $dto
     *
     * @return mixed
     */
    public function handle(DTOInterface $dto)
    {
        $passwordEncoded = new PasswordEncoded($dto->getPlainPassword());

        $user = new User($dto->getUsername(),$dto->getEmail(),$passwordEncoded);

        $this->userRepository->save($user);

        // TODO: Implement handle() method.
    }
}