<?php
/**
 * User: Oscar Sanchez
 * Date: 6/8/20
 */

namespace App\Service\Handler;


use App\Entity\ProjectImage;
use App\Entity\ValueObject\ProjectType;
use App\Exception\UserNotIsPropertyOfObjectException;
use App\Repository\ProjectRepositoryInterface;
use App\Repository\SkillRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\ProjectCreateDTO;

class ProjectUpdateHandler implements ServiceHandlerInterface
{
    /**
     * @var ProjectRepositoryInterface;
     */
    private $projectRepository;

    /**
     * @var SkillRepositoryInterface
     */
    private $skillRepository;

    /**
     * ProjectUpdateHandler constructor.
     * @param ProjectRepositoryInterface $projectRepository
     * @param SkillRepositoryInterface $skillRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository, SkillRepositoryInterface $skillRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->skillRepository = $skillRepository;
    }


    /**
     * @param DTOInterface|ProjectCreateDTO $dto
     * @return mixed|void
     * @throws UserNotIsPropertyOfObjectException
     */
    public function handle(DTOInterface $dto)
    {
        $project = $this->projectRepository->getById($dto->getId());

        if($project->getUser()->getId() != $dto->getUser()) {
            throw new UserNotIsPropertyOfObjectException();
        }

        $project->setName($dto->getName());
        $project->setDescription($dto->getDescription());
        $project->setDate($dto->getDate());
        $project->setFeatured($dto->isFeatured());
        $project->setShortDescription($dto->getShortDescription());
        $project->addMedia(
            $dto->getLogo(),
            $dto->getImageFull(),
            $dto->getFile(),
            $dto->getImageList(),
            $dto->getImageListPhone()
        );

        if($project->getType()->get() != $dto->getType() ) {
            $projectType = new ProjectType($dto->getType());
            $project->setType($projectType);
        }

        $project->setUrl($dto->getUrl());
        $project->setButtonFileText($dto->getButtonFileText());

        $arraySkills = [];

        foreach ($dto->getSkills() as $skillId) {
            $skill = $this->skillRepository->getById($skillId);
            $arraySkills[] = $skill;
        }

        if(!empty($arraySkills)) {
            $project->setSkills($arraySkills);
        }

        foreach($dto->getImages() as $image) {
            $projectImage = ProjectImage::createAndUploadFromProject($project,$image);
            $project->addImage($projectImage);
        }

        $this->projectRepository->save($project);
    }
}