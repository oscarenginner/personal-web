<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Service\Handler;


use App\Repository\SkillRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\SkillDeleteDTO;

class SkillDeleteHandler implements ServiceHandlerInterface
{
    /**
     * @var SkillRepositoryInterface
     */
    private $skillRepository;

    /**
     * SkillDeleteHandler constructor.
     * @param SkillRepositoryInterface $skillRepository
     */
    public function __construct(SkillRepositoryInterface $skillRepository)
    {
        $this->skillRepository = $skillRepository;
    }


    /**
     * @param DTOInterface|SkillDeleteDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $skill = $this->skillRepository->getById($dto->getId());
        $this->skillRepository->delete($skill);
    }
}