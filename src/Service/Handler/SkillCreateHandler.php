<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Service\Handler;


use App\Entity\Skill;
use App\Repository\SkillCategoryRepositoryInterface;
use App\Repository\SkillRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\SkillCreateDTO;
use App\Exception\SkillScoredNotInRangeException;

class SkillCreateHandler implements ServiceHandlerInterface
{

    /**
     * @var SkillRepositoryInterface
     */
    private $skillRepository;

    /**
     * @var SkillCategoryRepositoryInterface
     */
    private $skillCategoryRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * SkillCreateHandler constructor.
     * @param SkillRepositoryInterface $skillRepository
     * @param SkillCategoryRepositoryInterface $skillCategoryRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(SkillRepositoryInterface $skillRepository,
                                SkillCategoryRepositoryInterface $skillCategoryRepository,
                                UserRepositoryInterface $userRepository)
    {
        $this->skillRepository = $skillRepository;
        $this->skillCategoryRepository = $skillCategoryRepository;
        $this->userRepository = $userRepository;
    }


    /**
     * @param DTOInterface|SkillCreateDTO $dto
     * @return mixed|void
     * @throws SkillScoredNotInRangeException
     */
    public function handle(DTOInterface $dto)
    {
        $user = $this->userRepository->getById($dto->getUser());
        $skillCategory = $this->skillCategoryRepository->getById($dto->getSkillCategory());
        $skill = new Skill($dto->getName(), $skillCategory, $dto->getScore(), $user, $dto->getImage(),$dto->isFeatured());

        $this->skillRepository->save($skill);
    }
}