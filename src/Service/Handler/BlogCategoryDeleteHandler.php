<?php
/**
 * User: Oscar Sanchez
 * Date: 20/8/20
 */

namespace App\Service\Handler;


use App\Exception\UserNotIsPropertyOfObjectException;
use App\Repository\BlogCategoryRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Service\DTO\BlogCategoryDeleteDTO;
use App\Service\DTO\DTOInterface;

class BlogCategoryDeleteHandler implements ServiceHandlerInterface
{
    /**
     * @var BlogCategoryRepositoryInterface
     */
    private $blogCategoryRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * BlogCategoryDeleteHandler constructor.
     * @param BlogCategoryRepositoryInterface $blogCategoryRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(BlogCategoryRepositoryInterface $blogCategoryRepository,
                                UserRepositoryInterface $userRepository)
    {
        $this->blogCategoryRepository = $blogCategoryRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param DTOInterface|BlogCategoryDeleteDTO $dto
     * @return mixed|void
     * @throws UserNotIsPropertyOfObjectException
     */
    public function handle(DTOInterface $dto)
    {
        $blogCategory = $this->blogCategoryRepository->getById($dto->getId());

        if($dto->getUser() != $blogCategory->getUser()->getId()) {
            throw new UserNotIsPropertyOfObjectException();
        }

        if($blogCategory->getImage()) {
            $blogCategory->deleteImage();
        }

        $this->blogCategoryRepository->delete($blogCategory);

    }
}