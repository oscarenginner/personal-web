<?php
/**
 * User: Oscar Sanchez
 * Date: 28/7/20
 */

namespace App\Service\Handler;


use App\Repository\UserRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\UserUpdateDTO;

class UserUpdateHandler implements ServiceHandlerInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserUpdateHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    /**
     * @param DTOInterface|UserUpdateDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $user = $this->userRepository->getById($dto->getId());

        $user->setPhone($dto->getPhone());
        $user->setName($dto->getName());
        $user->setSurname($dto->getSurname());
        $user->setInstagramLink($dto->getInstagramLink());
        $user->setFacebookLink($dto->getFacebookLink());
        $user->setGithubLink($dto->getGithubLink());
        $user->setShortDescription($dto->getShortDescription());
        $user->setHistory($dto->getHistory());
        $user->setHackerRankLink($dto->getHackerRankLink());
        $user->setLinkedinLink($dto->getLinkedinLink());
        $user->setCodepenLink($dto->getCodepenLink());
        $user->setWhatsappLink($dto->getWhatsappLink());

        if($dto->getAvatar()) {
            $user->addAvatar($dto->getAvatar());
        }
        
        $this->userRepository->save($user);
    }
}