<?php
/**
 * User: Oscar Sanchez
 * Date: 30/7/20
 */

namespace App\Service\Handler;


use App\Entity\SkillCategory;
use App\Repository\SkillCategoryRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\SkillCategoryCreateDTO;

class SkillCategoryCreateHandler implements ServiceHandlerInterface
{
    /**
     * @var SkillCategoryRepositoryInterface
     */
    private $skillCategoryRepository;

    /**
     * SkillCategoryCreate constructor.
     * @param SkillCategoryRepositoryInterface $skillCategoryRepository
     */
    public function __construct(SkillCategoryRepositoryInterface $skillCategoryRepository)
    {
        $this->skillCategoryRepository = $skillCategoryRepository;
    }


    /**
     * @param DTOInterface|SkillCategoryCreateDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $skillCategory = new SkillCategory($dto->getName(),$dto->getIcon());
        $this->skillCategoryRepository->save($skillCategory);
    }
}