<?php
/**
 * User: Oscar Sanchez
 * Date: 4/8/20
 */

namespace App\Service\Handler;


use App\Doctrine\Repository\SkillRepository;
use App\Entity\Experience;
use App\Exception\ExperienceDataIsNotCorrectException;
use App\Repository\ExperienceRepositoryInterface;
use App\Repository\SkillRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\ExperienceCreateDTO;
use Exception;

class ExperienceCreateHandler implements ServiceHandlerInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var ExperienceRepositoryInterface
     */
    private $experienceRepository;

    /**
     * @var SkillRepositoryInterface;
     */
    private $skillRepository;

    /**
     * ExperienceCreateHandler constructor.
     * @param UserRepositoryInterface $userRepository
     * @param ExperienceRepositoryInterface $experienceRepository
     * @param SkillRepositoryInterface $skillRepository
     */
    public function __construct(UserRepositoryInterface $userRepository,
                                ExperienceRepositoryInterface $experienceRepository,
                                SkillRepositoryInterface $skillRepository)
    {
        $this->userRepository = $userRepository;
        $this->experienceRepository = $experienceRepository;
        $this->skillRepository = $skillRepository;
    }


    /**
     * @param DTOInterface|ExperienceCreateDTO $dto
     * @return mixed|void
     * @throws ExperienceDataIsNotCorrectException
     * @throws Exception
     */
    public function handle(DTOInterface $dto)
    {
        $user = $this->userRepository->getById($dto->getUser());

        if(($dto->getEndDate()) && ($dto->getInitDate() > $dto->getEndDate())) {
            throw new ExperienceDataIsNotCorrectException();
        }

        $experience = new Experience($dto->getCompany(),
            $dto->getInitDate(),
            $dto->getEndDate(),
            $dto->getImage(),
            $dto->getJobPosition(),
            $dto->getJobDescription(),
            $user);

        foreach ($dto->getSkills() as $skillID) {
            $skill = $this->skillRepository->getById($skillID);
            $experience->addISkill($skill);

        }

        $this->experienceRepository->save($experience);
    }
}