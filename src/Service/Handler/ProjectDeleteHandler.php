<?php
/**
 * User: Oscar Sanchez
 * Date: 6/8/20
 */

namespace App\Service\Handler;


use App\Exception\UserNotIsPropertyOfObjectException;
use App\Repository\ProjectRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\ProjectDeleteDTO;

class ProjectDeleteHandler implements ServiceHandlerInterface
{
    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepository;

    /**
     * ProjectDeleteHandler constructor.
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }


    /**
     * @param DTOInterface|ProjectDeleteDTO $dto
     * @return mixed|void
     * @throws UserNotIsPropertyOfObjectException
     */
    public function handle(DTOInterface $dto)
    {
        $project = $this->projectRepository->getById($dto->getId());

        if($project->getUser()->getId() != $dto->getUser()) {
            throw new UserNotIsPropertyOfObjectException();
        }

        $project->deleteMedia();
        $this->projectRepository->delete($project);
    }
}