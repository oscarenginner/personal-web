<?php
/**
 * User: Oscar Sanchez
 * Date: 21/8/20
 */

namespace App\Service\Handler;


use App\Entity\SliderHome;
use App\Repository\SliderHomeRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\SliderHomeCreateDTO;

class SliderHomeCreateHandler implements ServiceHandlerInterface
{
    /**
     * @var SliderHomeRepositoryInterface
     */
    private $sliderHomeRepository;

    /**
     * SliderHomeCreateHandler constructor.
     * @param SliderHomeRepositoryInterface $sliderHomeRepository
     */
    public function __construct(SliderHomeRepositoryInterface $sliderHomeRepository)
    {
        $this->sliderHomeRepository = $sliderHomeRepository;
    }


    /**
     * @param DTOInterface|SliderHomeCreateDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $position = $this->sliderHomeRepository->getPositionMax();

        $sliderHome = new SliderHome(
            $dto->getTitle(),
            $dto->getSubtitle(),
            $dto->getImage(),
            $dto->getImagePhone(),
            $dto->getVideoYoutubeLink(),
            $dto->getLink(),
            $dto->getButtonText(),
            $position + 1
        );

        $this->sliderHomeRepository->save($sliderHome);
    }
}