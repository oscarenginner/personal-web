<?php
/**
 * User: Oscar Sanchez
 * Date: 4/8/20
 */

namespace App\Service\Handler;


use App\Exception\UserNotIsPropertyOfObjectException;
use App\Repository\ExperienceRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\ExperienceDeleteDTO;

class ExperienceDeleteHandler implements ServiceHandlerInterface
{
    /**
     * @var ExperienceRepositoryInterface
     */
    private $experienceRepository;

    /**
     * ExperienceDeleteHandler constructor.
     * @param ExperienceRepositoryInterface $experienceRepository
     */
    public function __construct(ExperienceRepositoryInterface $experienceRepository)
    {
        $this->experienceRepository = $experienceRepository;
    }


    /**
     * @param DTOInterface|ExperienceDeleteDTO $dto
     * @return mixed|void
     * @throws UserNotIsPropertyOfObjectException
     */
    public function handle(DTOInterface $dto)
    {
       $experience = $this->experienceRepository->getById($dto->getId());

        if($dto->getUser() != $experience->getId()) {
            throw new UserNotIsPropertyOfObjectException();
        }

       if($experience->getImage()) {
           $experience->deleteImage();
       }

       $this->experienceRepository->delete($experience);
    }
}