<?php
/**
 * User: Oscar Sanchez
 * Date: 21/8/20
 */

namespace App\Service\Handler;


use App\Entity\SliderHome;
use App\Repository\SliderHomeRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\SliderHomeDeleteDTO;

class SliderHomeDeleteHandler implements ServiceHandlerInterface
{
    /**
     * @var SliderHomeRepositoryInterface
     */
    private $sliderHomeRepository;

    /**
     * SliderHomeDeleteHandler constructor.
     * @param SliderHomeRepositoryInterface $sliderHomeRepository
     */
    public function __construct(SliderHomeRepositoryInterface $sliderHomeRepository)
    {
        $this->sliderHomeRepository = $sliderHomeRepository;
    }


    /**
     * @param DTOInterface|SliderHomeDeleteDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $sliderHome = $this->sliderHomeRepository->getById($dto->getId());

        $sliders =  $this->sliderHomeRepository->getUpPosition($sliderHome->getPosition());

        $sliderHome->deleteMedia();

        $this->sliderHomeRepository->delete($sliderHome);


        /** @var SliderHome $slider */
        foreach ($sliders as $slider) {
            $position = $slider->getPosition();
            $position = $position - 1;
            $slider->setPosition($position);
            $this->sliderHomeRepository->save($slider);
        }

        
    }
}