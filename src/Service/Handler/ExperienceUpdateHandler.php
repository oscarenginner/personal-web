<?php
/**
 * User: Oscar Sanchez
 * Date: 5/8/20
 */

namespace App\Service\Handler;


use App\Exception\UserNotIsPropertyOfObjectException;
use App\Repository\ExperienceRepositoryInterface;
use App\Repository\SkillRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\ExperienceCreateDTO;

class ExperienceUpdateHandler implements ServiceHandlerInterface
{

    /**
     * @var ExperienceRepositoryInterface
     */
    private $experienceRepository;

    /**
     * @var SkillRepositoryInterface
     */
    private $skillRepository;

    /**
     * ExperienceUpdateHandler constructor.
     * @param ExperienceRepositoryInterface $experienceRepository
     * @param SkillRepositoryInterface $skillRepository
     */
    public function __construct(ExperienceRepositoryInterface $experienceRepository,
                                SkillRepositoryInterface $skillRepository)
    {
        $this->experienceRepository = $experienceRepository;
        $this->skillRepository = $skillRepository;
    }


    /**
     * @param DTOInterface|ExperienceCreateDTO $dto
     * @return mixed|void
     * @throws UserNotIsPropertyOfObjectException
     * @throws \Exception
     */
    public function handle(DTOInterface $dto)
    {
       $experience = $this->experienceRepository->getById($dto->getId());

       if($experience->getUser()->getId() != $dto->getUser()) {
           throw new UserNotIsPropertyOfObjectException();
       }

       $experience->setCompany($dto->getCompany());
       $experience->addDates($dto->getInitDate(),$dto->getEndDate());
       $experience->setJobPosition($dto->getJobPosition());
       $experience->setJobDescription($dto->getJobDescription());

       if($dto->getImage()) {
           $experience->addImage($dto->getImage());
       }

        $arraySkills = [];

       foreach ($dto->getSkills() as $skillId) {
           $skill = $this->skillRepository->getById($skillId);
           $arraySkills[] = $skill;
       }

       if(!empty($arraySkills)) {
           $experience->setSkills($arraySkills);
       }

       $this->experienceRepository->save($experience);

    }
}