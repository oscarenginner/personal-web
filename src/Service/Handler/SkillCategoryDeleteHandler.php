<?php
/**
 * User: Oscar Sanchez
 * Date: 2/8/20
 */

namespace App\Service\Handler;


use App\Repository\SkillCategoryRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\SkIllCategoryDeleteDTO;

class SkillCategoryDeleteHandler implements ServiceHandlerInterface
{
    /**
     * @var SkillCategoryRepositoryInterface
     */
    private $skillCategoryRepository;

    /**
     * SkillCategoryDeleteHandler constructor.
     * @param SkillCategoryRepositoryInterface $skillCategoryRepository
     */
    public function __construct(SkillCategoryRepositoryInterface $skillCategoryRepository)
    {
        $this->skillCategoryRepository = $skillCategoryRepository;
    }


    /**
     * @param DTOInterface|SkIllCategoryDeleteDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $skillCategory = $this->skillCategoryRepository->getById($dto->getId());

        $skillCategory->deleteIcon();
        $this->skillCategoryRepository->delete($skillCategory);
    }
}