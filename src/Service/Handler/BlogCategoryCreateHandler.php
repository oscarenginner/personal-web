<?php
/**
 * User: Oscar Sanchez
 * Date: 19/8/20
 */

namespace App\Service\Handler;


use App\Doctrine\Repository\UserRepository;
use App\Entity\BlogCategory;
use App\Repository\BlogCategoryRepositoryInterface;
use App\Service\DTO\BlogCategoryCreateDTO;
use App\Service\DTO\DTOInterface;

class BlogCategoryCreateHandler implements ServiceHandlerInterface
{
    /**
     * @var BlogCategoryRepositoryInterface
     */
    private $blogCategoryRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * BlogCategoryHandler constructor.
     * @param BlogCategoryRepositoryInterface $blogCategoryRepository
     * @param UserRepository $userRepository
     */
    public function __construct(BlogCategoryRepositoryInterface $blogCategoryRepository, UserRepository $userRepository)
    {
        $this->blogCategoryRepository = $blogCategoryRepository;
        $this->userRepository = $userRepository;
    }


    /**
     * @param DTOInterface|BlogCategoryCreateDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $user = $this->userRepository->getById($dto->getUser());

        $blogCategory = new BlogCategory(
            $dto->getTitle(),
            $dto->getSubtitle(),
            $dto->getContent(),
            $dto->getMetaTitle(),
            $dto->getMetaDescription(),
            $dto->getImage(),
            $user);

        $this->blogCategoryRepository->save($blogCategory);
    }
}