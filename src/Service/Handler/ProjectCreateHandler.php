<?php
/**
 * User: Oscar Sanchez
 * Date: 5/8/20
 */

namespace App\Service\Handler;


use App\Entity\Project;
use App\Entity\ProjectImage;
use App\Entity\ValueObject\ProjectType;
use App\Repository\ProjectRepositoryInterface;
use App\Repository\SkillRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\ProjectCreateDTO;

class ProjectCreateHandler implements ServiceHandlerInterface
{
    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var SkillRepositoryInterface
     */
    private $skillRepository;

    /**
     * ProjectCreateHandler constructor.
     * @param ProjectRepositoryInterface $projectRepository
     * @param UserRepositoryInterface $userRepository
     * @param SkillRepositoryInterface $skillRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository,
                                UserRepositoryInterface $userRepository,
                                SkillRepositoryInterface $skillRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->userRepository = $userRepository;
        $this->skillRepository = $skillRepository;
    }


    /**
     * @param DTOInterface|ProjectCreateDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $user = $this->userRepository->getById($dto->getUser());

        $projectType = new ProjectType($dto->getType());
        $project = new Project(
            $projectType,
            $dto->getName(),
            $dto->getDescription(),
            $dto->getLogo(),
            $dto->getDate(),
            $dto->getImageFull(),
            $user,
            $dto->getUrl(),
            $dto->getFile(),
            $dto->getButtonFileText(),
            $dto->getShortDescription(),
            $dto->getImageList(),
            $dto->getImageListPhone() ,
            $dto->isFeatured());


        foreach ($dto->getSkills() as $skillID) {
            $skill = $this->skillRepository->getById($skillID);
            $project->addISkill($skill);
        }

        $this->projectRepository->save($project);

        foreach($dto->getImages() as $image) {
            $projectImage = ProjectImage::createAndUploadFromProject($project,$image);
            $project->addImage($projectImage);
        }

        $this->projectRepository->save($project);
    }
}