<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Service\Handler;


use App\Exception\SkillScoredNotInRangeException;
use App\Exception\UserNotIsPropertyOfObjectException;
use App\Repository\SkillCategoryRepositoryInterface;
use App\Repository\SkillRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\SkillUpdateDTO;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SkillUpdateHandler implements ServiceHandlerInterface
{
    /**
     * @var SkillRepositoryInterface
     */
    private $skillRepository;

    /**
     * @var SkillCategoryRepositoryInterface
     */
    private $skillCategoryRepository;

    /**
     * SkillCreateHandler constructor.
     * @param SkillRepositoryInterface $skillRepository
     * @param SkillCategoryRepositoryInterface $skillCategoryRepository
     */
    public function __construct(SkillRepositoryInterface $skillRepository,
                                SkillCategoryRepositoryInterface $skillCategoryRepository)
    {
        $this->skillRepository = $skillRepository;
        $this->skillCategoryRepository = $skillCategoryRepository;
    }


    /**
     * @param DTOInterface|SkillUpdateDTO $dto
     * @return mixed|void
     * @throws UserNotIsPropertyOfObjectException
     * @throws SkillScoredNotInRangeException
     */
    public function handle(DTOInterface $dto)
    {
        $skill = $this->skillRepository->getById($dto->getId());

        if($dto->getUser() != $skill->getUser()->getId()) {
            throw new UserNotIsPropertyOfObjectException();
        }

        $skill->setName($dto->getName());
        $skill->setScore($dto->getScore());

        if($dto->getSkillCategory() != $skill->getSkillCategory()->getId()) {

            $skillCategory = $this->skillCategoryRepository->getById($dto->getSkillCategory());
            $skill->setSkillCategory($skillCategory);
        }

        $skill->setFeatured($dto->isFeatured());

        if($dto->getImage()) {
            $skill->addImage($dto->getImage());
        }

        $this->skillRepository->save($skill);

    }
}