<?php
/**
 * User: Oscar Sanchez
 * Date: 20/8/20
 */

namespace App\Service\Handler;


use App\Entity\Post;
use App\Repository\BlogCategoryRepositoryInterface;
use App\Repository\PostRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\PostCreateDTO;

class PostCreateHandler implements ServiceHandlerInterface
{
    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var BlogCategoryRepositoryInterface
     */
    private $blogCategoryRepository;

    /**
     * PostCreateHandler constructor.
     * @param PostRepositoryInterface $postRepository
     * @param UserRepositoryInterface $userRepository
     * @param BlogCategoryRepositoryInterface $blogCategoryRepository
     */
    public function __construct(PostRepositoryInterface $postRepository,
                                UserRepositoryInterface $userRepository,
                                BlogCategoryRepositoryInterface $blogCategoryRepository)
    {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
        $this->blogCategoryRepository = $blogCategoryRepository;
    }


    /**
     * @param DTOInterface|PostCreateDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $user = $this->userRepository->getById($dto->getAuthor());
        $category = $this->blogCategoryRepository->getById($dto->getCategory());

        $post = new Post(
            $dto->getTitle(),
            $dto->getSubtitle(),
            $dto->getContent(),
            $user,
            $dto->getImage(),
            $dto->getImageList(),
            $category,
            $dto->getExcerpt(),
            $dto->getMetaTitle(),
            $dto->getMetaDescription()
        );

        if($dto->isPublish()) {
            $post->publish();
        }

        $this->postRepository->save($post);
    }
}