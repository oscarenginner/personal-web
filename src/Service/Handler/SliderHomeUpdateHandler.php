<?php
/**
 * User: Oscar Sanchez
 * Date: 22/8/20
 */

namespace App\Service\Handler;


use App\Repository\SliderHomeRepositoryInterface;
use App\Service\DTO\DTOInterface;
use App\Service\DTO\SliderHomeUpdateDTO;

class SliderHomeUpdateHandler implements ServiceHandlerInterface
{
    /**
     * @var SliderHomeRepositoryInterface
     */
    private $sliderHomeRepository;

    /**
     * SliderHomeUpdateHandler constructor.
     * @param SliderHomeRepositoryInterface $sliderHomeRepository
     */
    public function __construct(SliderHomeRepositoryInterface $sliderHomeRepository)
    {
        $this->sliderHomeRepository = $sliderHomeRepository;
    }


    /**
     * @param DTOInterface|SliderHomeUpdateDTO $dto
     * @return mixed|void
     */
    public function handle(DTOInterface $dto)
    {
        $sliderHome = $this->sliderHomeRepository->getById($dto->getId());

        $sliderHome->setTitle($dto->getTitle());
        $sliderHome->setSubtitle($dto->getSubtitle());
        $sliderHome->setLink($dto->getLink());
        $sliderHome->setVideoYoutubeLink($dto->getVideoYoutubeLink());
        $sliderHome->setTextButton($dto->getButtonText());

        if($dto->getImage()) {
            $sliderHome->addImage($dto->getImage());
        }

        if($dto->getImagePhone()) {
            $sliderHome->addImagePhone($dto->getImagePhone());
        }

        $this->sliderHomeRepository->save($sliderHome);
    }
}