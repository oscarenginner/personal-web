<?php

namespace App\Service\Query;

class PagedQueryResult
{
    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $count;

    /**
     * @var array
     */
    private $result;

    /**
     * PagedQueryResult constructor.
     *
     * @param int   $page
     * @param int   $limit
     * @param int   $count
     * @param array $result
     */
    public function __construct(int $page, int $limit, int $count, array $result)
    {
        $this->page = $page;
        $this->limit = $limit;
        $this->count = $count;
        $this->result = $result;
    }

    /**
     * @return int
     */
    public function page(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function firstPage(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function numberOfPages(): int
    {
        return ceil($this->count() / $this->limit());
    }

    /**
     * @return int
     */
    public function start(): int
    {
        return $this->page() > 1 ? (($this->page() - 1) * $this->limit()) + 1 : 1;
    }

    /**
     * @return int
     */
    public function end(): int
    {
        return ($this->start() + count($this->result())) - 1;
    }

    /**
     * @return array
     */
    public function result(): array
    {
        return $this->result;
    }
}
