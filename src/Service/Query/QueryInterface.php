<?php
namespace App\Service\Query;

use App\Service\DTO\DTOInterface;

interface QueryInterface
{
    /**
     * @param DTOInterface $query
     * @return mixed
     */
    public function query(DTOInterface $query);
}