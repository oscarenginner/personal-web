<?php
namespace App\Service\Query;


use App\Repository\PostRepositoryInterface;
use App\Service\DTO\BlogQueryDTO;
use App\Service\DTO\DTOInterface;

class BlogQuery implements QueryInterface
{
    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * BlogQuery constructor.
     * @param PostRepositoryInterface $postRepository
     */
    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }


    /**
     * @param DTOInterface|BlogQueryDTO $query
     * @return mixed|void
     */
    public function query(DTOInterface $query)
    {
      return $this->postRepository->findByQuery($query);
    }
}