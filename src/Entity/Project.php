<?php
/**
 * User: Oscar Sanchez
 * Date: 5/8/20
 */

namespace App\Entity;


use App\Entity\ValueObject\ProjectType;
use App\Image\ImageManipulator;
use App\Util\StringUtil;
use App\Util\UploadsUtil;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;

class Project
{
    const UPLOAD_SUBDIRECTORY = 'project';
    /**
     * @var int
     */
    private $id;

    /**
     * @var ProjectType
     */
    private $type;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $logo;

    /**
     * @var DateTimeInterface
     */
    private $date;

    /**
     * @var string
     */
    private $imageFull;

    /**
     * @var ArrayCollection
     */
    private $skills;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $file;

    /**
     * @var string|null
     */
    private $buttonFileText;

    /**
     * @var ArrayCollection
     */
    private $images;

    /**
     * @var string
     */
    private $shortDescription;

    /**
     * @var string
     */
    private $imageList;

    /**
     * @var string
     */
    private $imageListPhone;

    /**
     * @var bool
     */
    private $featured;


    /**
     * Project constructor.
     * @param ProjectType $type
     * @param string $name
     * @param string $description
     * @param File|null $logo
     * @param DateTimeInterface $date
     * @param File|null $imageFull
     * @param User $user
     * @param string $url
     * @param File|null $file
     * @param string $buttonFileText
     * @param string $shortDescription
     * @param File|null $imageList
     * @param File|null $imageListPhone
     * @param bool $featured
     */
    public function __construct(ProjectType $type,
                                string $name,
                                string $description,
                                ?File $logo,
                                DateTimeInterface $date,
                                ?File $imageFull,
                                User $user, string $url ,
                                ?File $file,
                                string $buttonFileText ,
                                string $shortDescription,
                                ?File $imageList,
                                ?File $imageListPhone ,
                                bool $featured)
    {
        $this->type = $type;
        $this->name = $name;
        $this->description = $description;
        $this->date = $date;
        $this->imageFull = $imageFull;
        $this->skills = new ArrayCollection();
        $this->user = $user;
        $this->url = $url;
        $this->addMedia($logo,$imageFull,$file, $imageList, $imageListPhone);
        $this->buttonFileText = $buttonFileText;
        $this->images = new ArrayCollection();
        $this->shortDescription = $shortDescription;
        $this->featured = $featured;
    }


    /**
     * @param File|null $logo
     * @param File|null $imageFull
     * @param File|null $file
     * @param File|null $imageList
     * @param File|null $imageListPhone
     */
    public function addMedia(?File $logo, ?File $imageFull,?File $file, ?File $imageList , ?File $imageListPhone)
    {
        if($logo) {
            $this->addLogo($logo);
        }

        if($imageFull) {
            $this->addImageFull($imageFull);
        }

        if($file) {
            $this->addFile($file);
        }

        if($imageList) {
            $this->addImageList($imageList);
        }

        if($imageListPhone) {
            $this->addImageListPhone($imageListPhone);
        }
    }

    /**
     * @param string $file
     */
    private function addImageFull(string $file)
    {
        if (isset($this->imageFull)) {
            $this->deleteImageFull();
        }

        $cleanString = StringUtil::cleanString($this->name);
        $fileName = $cleanString . '-' . uniqid() . '.';
        $extension = ImageManipulator::getExtensionTmpFile($file);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName;

        ImageManipulator::generateFullCover($fileOutput, $file);

        $this->imageFull = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $extension;
    }

    /**
     *
     */
    private function deleteImageFull(): void
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->imageFull));

        } catch (\InvalidArgumentException $e) {
        }


        $this->imageFull = null;
    }

    /**
     * @param string $file
     */
    private function addLogo(string $file)
    {
        if (isset($this->logo)) {
            $this->deleteLogo();
        }

        $cleanString = StringUtil::cleanString($this->name);
        $fileName = $cleanString . '-logo-' . uniqid() . '.';
        $extension = ImageManipulator::getExtensionTmpFile($file);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName;

        ImageManipulator::generateThumbnails($fileOutput, $file);

        $this->logo = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $extension;
    }

    /**
     *
     */
    private function deleteLogo(): void
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->logo));

        } catch (\InvalidArgumentException $e) {
        }


        $this->logo = null;
    }

    /**
     * @param Skill $skill
     */
    public function addISkill(Skill $skill): void
    {
        $this->skills->add($skill);
    }

    public function addImage(ProjectImage $projectImage) : void
    {
        $this->images->add($projectImage);
    }

    /**
     * @return array
     */
    public function getSkills()
    {
        return $this->skills->toArray();
    }

    /**
     * @param array $skills
     */
    public function setSkills(array $skills)
    {
        $this->skills = new ArrayCollection();

        foreach ($skills as $skill) {
            $this->skills->add($skill);
        }
    }

    /**
     * @return array
     */
    public function getSkillToPairValuesArray()
    {
        $skillsArray = [];
        foreach($this->skills as $skill) {
            $skillsArray[$skill->getName()] = $skill->getId();
        }
        return $skillsArray;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ProjectType
     */
    public function getType(): ProjectType
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getImageFull(): string
    {
        return $this->imageFull;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @param File $file
     */
    private function addFile(File $file) : void
    {
        if($this->file) {
            $this->deleteFile();
        }
        $cleanString = StringUtil::cleanString($this->name);
        $fileName = $cleanString.'-file-'.uniqid().'.';

        UploadsUtil::upload($file,self::UPLOAD_SUBDIRECTORY,$fileName.$file->getClientOriginalExtension());

        $this->file = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $file->getClientOriginalExtension();
    }

    /**
     *
     */
    private function deleteFile() : void
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->file));

        } catch (\InvalidArgumentException $e) {
        }
    }

    /**
     * @return string|null
     */
    public function getButtonFileText(): ?string
    {
        return $this->buttonFileText;
    }

    /**
     * @return string
     */
    public function getYearRelease() : string
    {
        return $this->date->format('Y');
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     *
     */
    public function deleteMedia() : void
    {
        if($this->logo) {
            $this->deleteLogo();
        }

        if($this->imageFull) {
            $this->deleteImageFull();
        }

        if($this->file) {
            $this->deleteFile();
        }

        if($this->imageList) {
            $this->deleteImageList();
        }

        if($this->imageListPhone) {
            $this->deleteImageListPhone();
        }

    }

    /**
     * @param ProjectType $type
     */
    public function setType(ProjectType $type): void
    {
        $this->type = $type;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param DateTimeInterface $date
     */
    public function setDate(DateTimeInterface $date): void
    {
        $this->date = $date;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @param string $buttonFileText
     */
    public function setButtonFileText(string $buttonFileText): void
    {
        $this->buttonFileText = $buttonFileText;
    }

    /**
     * @param array $skills
     */
    public function setSkill(array $skills) : void
    {
        $this->skills = new ArrayCollection();

        foreach ($skills as $skill) {
            $this->skills->add($skill);
        }
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images->toArray();
    }

    /**
     * @param int $imageId
     */
    public function removeImage(int $imageId) : void
    {
        $imagesFilter = $this->images->filter(function(ProjectImage $projectImage) use ($imageId) {
            if($projectImage->getId() === $imageId) {
                return $projectImage;
            }
        });

        if(empty($imagesFilter)) {

            return;
        }

        /** @var ProjectImage $projectImage */
        $projectImage = $imagesFilter->first();
        $projectImage->deleteMedias();
        $this->images->removeElement($projectImage);
    }

    /**
     * @param string $file
     */
    public function addImageList(string $file)  : void
    {
        if (isset($this->imageList)) {
            $this->deleteImageList();
        }

        $cleanString = StringUtil::cleanString($this->name);
        $fileName = $cleanString . '-imagelist-' . uniqid() . '.';
        $extension = ImageManipulator::getExtensionTmpFile($file);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName.$extension;

        ImageManipulator::generateThumbnails($fileOutput, $file);

        ImageManipulator::resize_crop_image(900,600,$file,$fileOutput);

        $this->imageList = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $extension;
    }

    /**
     *
     */
    private function deleteImageList(): void
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->imageList));

        } catch (\InvalidArgumentException $e) {
        }

        $this->imageList = null;
    }

    /**
     * @param File $file
     */
    private function addImageListPhone(File $file)
    {
        if (isset($this->imageListPhone)) {
            $this->deleteImageListPhone();
        }

        $cleanString = StringUtil::cleanString($this->name);
        $fileName = $cleanString. '-imagelistphone' . uniqid() . '.';
        $extension = ImageManipulator::getExtensionTmpFile($file);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName.$extension;

        ImageManipulator::generateThumbnails($fileOutput, $file);

        ImageManipulator::resize_crop_image(400,400,$file,$fileOutput);

        $this->imageListPhone = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $extension;
    }

    /**
     *
     */
    private function deleteImageListPhone()
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->imageListPhone));

        } catch (\InvalidArgumentException $e) {
        }

        $this->imageListPhone = null;
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @return string
     */
    public function getImageList(): string
    {
        return $this->imageList;
    }

    /**
     * @return string
     */
    public function getImageListPhone(): string
    {
        return $this->imageListPhone;
    }

    /**
     * @param string $shortDescription
     */
    public function setShortDescription(string $shortDescription): void
    {
        $this->shortDescription = $shortDescription;

    }

    /**
     * @return bool
     */
    public function isFeatured(): bool
    {
        return $this->featured;
    }

    /**
     * @param bool $featured
     */
    public function setFeatured(bool $featured): void
    {
        $this->featured = $featured;
    }

}