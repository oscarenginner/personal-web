<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Entity;

use App\Exception\ExperienceDataIsNotCorrectException;
use App\Image\ImageManipulator;
use App\Util\DateUtil;
use App\Util\UploadsUtil;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use Symfony\Component\HttpFoundation\File\File;

class Experience
{
    const UPLOAD_SUBDIRECTORY = 'experience';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $company;

    /**
     * @var DateTimeInterface
     */
    private $initDate;

    /**
     * @var DateTimeInterface|null
     */
    private $endDate;

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $jobPosition;

    /**
     * @var string
     */
    private $jobDescription;

    /**
     * @var User
     */
    private $user;

    /**
     * @var ArrayCollection|Skill
     */
    private $skills;

    /**
     * Experience constructor.
     * @param string $company
     * @param DateTimeInterface $initData
     * @param DateTimeInterface|null $endDate
     * @param File $image
     * @param string $jobPosition
     * @param string $jobDescription
     * @param User $user
     * @throws Exception
     */
    public function __construct(string $company,
                                DateTimeInterface $initData,
                                ?DateTimeInterface $endDate,
                                File $image,
                                string $jobPosition,
                                string $jobDescription,
                                User $user)
    {
        $this->company = $company;
        $this->jobPosition = $jobPosition;
        $this->jobDescription = $jobDescription;
        $this->user = $user;
        $this->skills = new ArrayCollection();

        $this->addDates($initData, $endDate);

        if ($image) {
            $this->addImage($image);
        }
    }

    /**
     * @param DateTimeInterface $initDate
     * @param DateTimeInterface|null $endDate
     * @throws Exception
     */
    public function addDates(DateTimeInterface $initDate, ?DateTimeInterface $endDate)
    {
        $this->initDate = $initDate;

        if (!$endDate) {
            return;
        }

        if ($initDate > $endDate) {
            throw new ExperienceDataIsNotCorrectException();
        }

        $this->endDate = $endDate;
    }

    /**
     * @param string $file
     */
    public function addImage(string $file)
    {
        if (isset($this->image)) {
            $this->deleteImage();
        }

        $fileName = $this->company . '-' . uniqid() . '.';
        $extension = ImageManipulator::getExtensionTmpFile($file);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName;

        ImageManipulator::generateThumbnails($fileOutput, $file);

        $this->image = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $extension;
    }

    /**
     *
     */
    public function deleteImage(): void
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->image));

        } catch (\InvalidArgumentException $e) {
        }


        $this->image = null;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getInitDateFormat(string $format = '%B %G'): string
    {
        return DateUtil::getDateFormatLangFromDateTime($this->initDate, $format);
    }

    /**
     * @param string $format
     * @return string
     */
    public function getEndDateFormat(string $format = '%B %G'): string
    {
        if (!$this->endDate) {
            return '-';
        }

        return DateUtil::getDateFormatLangFromDateTime($this->endDate, $format);
    }

    public function getDuration()
    {
        if (!$this->endDate) {

            $endDate = new \DateTime('now');
            return DateUtil::dateDiff($this->initDate, $endDate);
        }

        return DateUtil::dateDiff($this->initDate, $this->endDate);

    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getEndDate(): ?DateTimeInterface
    {
        return $this->endDate;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getJobPosition(): string
    {
        return $this->jobPosition;
    }

    /**
     * @return string
     */
    public function getJobDescription(): string
    {
        return $this->jobDescription;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param Skill $skill
     */
    public function addISkill(Skill $skill): void
    {
        $this->skills->add($skill);
    }

    /**
     * @return DateTimeInterface
     */
    public function getInitDate(): DateTimeInterface
    {
        return $this->initDate;
    }

    /**
     * @return array
     */
    public function getSkills()
    {
        return $this->skills->toArray();
    }

    /**
     * @param array $skills
     */
    public function setSkills(array $skills)
    {
        $this->skills = new ArrayCollection();

        foreach ($skills as $skill) {
            $this->skills->add($skill);
        }
    }

    /**
     * @param string $company
     */
    public function setCompany(string $company): void
    {
        $this->company = $company;
    }

    /**
     * @param string $jobPosition
     */
    public function setJobPosition(string $jobPosition): void
    {
        $this->jobPosition = $jobPosition;
    }

    /**
     * @param string $jobDescription
     */
    public function setJobDescription(string $jobDescription): void
    {
        $this->jobDescription = $jobDescription;
    }

    /**
     * @return array
     */
    public function getSkillToPairValuesArray()
    {
        $skillsArray = [];
        foreach($this->skills as $skill) {
            $skillsArray[$skill->getName()] = $skill->getId();
        }
        return $skillsArray;
    }

}