<?php
/**
 * User: Oscar Sanchez
 * Date: 21/8/20
 */

namespace App\Entity;


use App\Image\ImageManipulator;
use App\Util\UploadsUtil;
use Symfony\Component\HttpFoundation\File\File;

class SliderHome
{
    const UPLOAD_SUBDIRECTORY = 'slider';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $subtitle;

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $videoYoutubeLink;

    /**
     * @var string
     */
    private $position;

    /**
     * @var string
     */
    private $imagePhone;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $textButton;

    /**
     * SliderHome constructor.
     * @param string $title
     * @param string $subtitle
     * @param File|null $image
     * @param File|null $imagePhone
     * @param string $videoYoutubeLink
     * @param string $link
     * @param string $textButton
     * @param string $position
     */
    public function __construct(string $title,
                                string $subtitle,
                                ?File $image,
                                ?File $imagePhone,
                                string $videoYoutubeLink,
                                string $link,
                                string $textButton,
                                string $position)
    {
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->videoYoutubeLink = $videoYoutubeLink;
        $this->link = $link;
        $this->textButton = $textButton;
        $this->position = $position;

        if($image) {
            $this->addImage($image);
        }

        if($imagePhone) {
            $this->addImagePhone($imagePhone);
        }
    }

    /**
     * @param string $file
     */
    public function addImage(string $file) : void
    {
        if (isset($this->image)) {
            $this->deleteImage();
        }

        $fileName = $this->title . '-' . uniqid() . '.';
        $extension = ImageManipulator::getExtensionTmpFile($file);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName;

        ImageManipulator::generateFullCover($fileOutput, $file);

        $this->image = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $extension;
    }

    /**
     *
     */
    private function deleteImage(): void
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->image));

        } catch (\InvalidArgumentException $e) {
        }


        $this->image = null;
    }

    /**
     * @param string $file
     */
    public function addImagePhone(string $file)  : void
    {
        if (isset($this->imagePhone)) {
            $this->removeImagePhone();
        }

        $fileName = $this->title . '-' . uniqid() . '.';
        $extension = ImageManipulator::getExtensionTmpFile($file);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName;

        ImageManipulator::generateThumbnails($fileOutput, $file);

        $this->imagePhone = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $extension;
    }

    /**
     *
     */
    private function removeImagePhone(): void
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->imagePhone));

        } catch (\InvalidArgumentException $e) {
        }

        $this->imagePhone = null;
    }

    /**
     *
     */
    public function deleteMedia() : void
    {
        if($this->image) {
            $this->deleteImage();
        }

        if($this->imagePhone) {
            $this->removeImagePhone();
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * @return string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getVideoYoutubeLink(): string
    {
        return $this->videoYoutubeLink;
    }

    /**
     * @return string
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function getImagePhone(): ?string
    {
        return $this->imagePhone;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getTextButton(): string
    {
        return $this->textButton;
    }

    public function setPosition(int $position) : void
    {
        $this->position = $position;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @param string $videoYoutubeLink
     */
    public function setVideoYoutubeLink(string $videoYoutubeLink): void
    {
        $this->videoYoutubeLink = $videoYoutubeLink;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @param string $textButton
     */
    public function setTextButton(string $textButton): void
    {
        $this->textButton = $textButton;
    }






}