<?php
/**
 * User: Oscar Sanchez
 * Date: 19/8/20
 */

namespace App\Entity;


use App\Image\ImageManipulator;
use App\Util\DateUtil;
use App\Util\UploadsUtil;
use DateTime;
use DateTimeInterface;
use Symfony\Component\DomCrawler\Image;
use Symfony\Component\HttpFoundation\File\File;

class Post
{
    const UPLOAD_SUBDIRECTORY = 'post_images';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $subtitle;

    /**
     * @var string
     */
    private $content;

    /**
     * @var DateTimeInterface
     */
    private $publishedAt;

    /**
     * @var bool
     */
    private $isPublished;

    /**
     * @var DateTimeInterface
     */
    private $createdAt;

    /**
     * @var User
     */
    private $author;

    /**
     * @var string|null;
     */
    private $image;

    /**
     * @var string|null
     */
    private $imageList;

    /**
     * @var BlogCategory
     */
    private $category;

    /**
     * @var string
     */
    private $excerpt;

    /**
     * @var string
     */
    private $metaTitle;

    /**
     * @var string
     */
    private $metaDescription;

    /**
     * Blog constructor.
     * @param string $title
     * @param string $subtitle
     * @param string $content
     * @param User $author
     * @param File|null $image
     * @param File|null $imageList
     * @param BlogCategory $category
     * @param string $excerpt
     * @param string $metaTitle
     * @param string $metaDescription
     */
    public function __construct(string $title,
                                string $subtitle,
                                string $content,
                                User $author,
                                ?File $image,
                                ?File $imageList,
                                BlogCategory $category,
                                string $excerpt,
                                string $metaTitle,
                                string $metaDescription)
    {
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->content = $content;
        $this->author = $author;
        $this->category = $category;
        $this->excerpt = $excerpt;

        $this->createdAt = new DateTime();

        if($image) {
            $this->addImage($image);
        }

        if($imageList) {
            $this->addImageList($imageList);
        }

        $this->unPublish();

        $this->metaTitle = $metaTitle;
        $this->metaDescription = $metaDescription;
    }

    /**
     * @param string $file
     */
    public function addImage(string $file) : void
    {
        if (isset($this->image)) {
            $this->deleteImage();
        }

        $fileName = $this->title . '-' . uniqid() . '.';
        $extension = ImageManipulator::getExtensionTmpFile($file);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName;

        ImageManipulator::generateFullCover($fileOutput, $file);

        $this->image = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $extension;
    }

    /**
     *
     */
    private function deleteImage(): void
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->image));

        } catch (\InvalidArgumentException $e) {
        }


        $this->image = null;
    }

    /**
     * @param string $file
     */
    public function addImageList(string $file)  : void
    {
        if (isset($this->imageList)) {
            $this->deleteImageList();
        }

        $fileName = $this->title . '-' . uniqid() . '.';
        $extension = ImageManipulator::getExtensionTmpFile($file);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName.$extension;

        ImageManipulator::generateThumbnails($fileOutput, $file);

        ImageManipulator::resize_crop_image(500,500,$file,$fileOutput);

        $this->imageList = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $extension;
    }

    /**
     *
     */
    private function deleteImageList(): void
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->imageList));

        } catch (\InvalidArgumentException $e) {
        }

        $this->imageList = null;
    }

    /**
     *
     */
    public function deleteMedias() : void
    {
        if($this->image) {
            $this->deleteImage();
        }

        if($this->imageList) {
            $this->deleteImageList();
        }
    }

    /**
     *
     */
    public function unPublish()
    {
        $this->isPublished = false;
        $this->publishedAt = null;
    }

    public function publish()
    {
        if($this->isPublished) {
            return;
        }

        $this->isPublished = true;
        $this->publishedAt = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return DateTimeInterface
     */
    public function getPublishedAt(): DateTimeInterface
    {
        return $this->publishedAt;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return DateUtil::getDateFormatByDateTime($this->createdAt);
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return string|null
     */
    public function getImageList(): ?string
    {
        return $this->imageList;
    }

    /**
     * @return BlogCategory
     */
    public function getCategory(): BlogCategory
    {
        return $this->category;
    }

    /**
     * @return string
     */
    public function getExcerpt(): string
    {
        return $this->excerpt;
    }

    /**
     * @return string
     */
    public function getMetaTitle(): string
    {
        return $this->metaTitle;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @param BlogCategory $category
     */
    public function setCategory(BlogCategory $category): void
    {
        $this->category = $category;
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt(string $excerpt): void
    {
        $this->excerpt = $excerpt;
    }

    /**
     * @param string $metaTitle
     */
    public function setMetaTitle(string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription(string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }











}