<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Entity;


use App\Exception\SkillScoredNotInRangeException;
use App\Image\ImageManipulator;
use App\Util\UploadsUtil;
use Symfony\Component\HttpFoundation\File\File;

class Skill
{
    const MINIM_SCORE = 0;

    const MAX_SCORE = 100;

    const UPLOAD_SUBDIRECTORY = 'skill';
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var SkillCategory
     */
    private $skillCategory;

    /**
     * @var int
     */
    private $score;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $image;

    /**
     * @var boolean
     */
    private $featured;

    /**
     * Skill constructor.
     * @param string $name
     * @param SkillCategory $skillCategory
     * @param int $score
     * @param User $user
     * @param File|null $image
     * @param bool $featured
     * @throws SkillScoredNotInRangeException
     */
    public function __construct(string $name,
                                SkillCategory $skillCategory,
                                int $score,
                                User $user,
                                ?File $image,
                                bool $featured)
    {
        $this->name = $name;
        $this->skillCategory = $skillCategory;
        $this->setScore($score);
        $this->user = $user;
        $this->featured = $featured;
        
        if($image) {
            $this->addImage($image);
        }
    }


    /**
     * @param int $score
     * @throws SkillScoredNotInRangeException
     */
    public function setScore(int $score): void
    {
        if(self::MINIM_SCORE > $score || self::MAX_SCORE < $score ) {
            throw new SkillScoredNotInRangeException();
        }
        $this->score = $score;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return SkillCategory
     */
    public function getSkillCategory(): SkillCategory
    {
        return $this->skillCategory;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param SkillCategory $skillCategory
     */
    public function setSkillCategory(SkillCategory $skillCategory): void
    {
        $this->skillCategory = $skillCategory;
    }

    public function addImage(File $image)
    {
        if (isset($this->image)) {
            $this->deleteImage();
        }

        $fileName = $this->name . '-' . uniqid() . '.';
        $extension = ImageManipulator::getExtensionTmpFile($image);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName;

        ImageManipulator::resize_crop_image(500,500,$image,$fileOutput.$extension);

        $this->image = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $extension;
    }

    private function deleteImage()
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->image));

        } catch (\InvalidArgumentException $e) {
        }


        $this->image = null;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return bool
     */
    public function isFeatured(): bool
    {
        if(!$this->featured) {
            return false;
        }
        return $this->featured;
    }

    /**
     * @param bool $featured
     */
    public function setFeatured(bool $featured): void
    {
        $this->featured = $featured;
    }

}