<?php
/**
 * User: Oscar Sanchez
 * Date: 19/8/20
 */

namespace App\Entity;


use App\Image\ImageManipulator;
use App\Util\StringUtil;
use App\Util\UploadsUtil;
use Symfony\Component\HttpFoundation\File\File;

class BlogCategory
{
    const UPLOAD_SUBDIRECTORY = 'blog_category';
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $subtitle;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $metaTitle;

    /**
     * @var string
     */
    private $metaDescription;

    /**
     * @var string
     */
    private $image;

    /**
     * @var User
     */
    private $user;

    /**
     * BlogCategory constructor.
     * @param string $title
     * @param string $subtitle
     * @param string $content
     * @param string $metaTitle
     * @param string $metaDescription
     * @param File $image
     * @param User $user
     */
    public function __construct(string $title,
                                string $subtitle,
                                string $content,
                                string $metaTitle,
                                string $metaDescription,
                                File $image,
                                User $user)
    {
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->content = $content;
        $this->metaTitle = $metaTitle;
        $this->metaDescription = $metaDescription;

        if ($image) {
            $this->addImage($image);
        }
        $this->user = $user;
    }

    /**
     * @param string $file
     */
    public function addImage(string $file)
    {
        if (isset($this->image)) {
            $this->deleteImage();
        }

        $fileName = $this->title . '-' . uniqid() . '.';
        $extension = ImageManipulator::getExtensionTmpFile($file);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName;

        ImageManipulator::generateFullCover($fileOutput, $file);

        $this->image = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY) . '/' . $fileName . $extension;
    }

    /**
     *
     */
    public function deleteImage(): void
    {
        try {
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->image));

        } catch (\InvalidArgumentException $e) {
        }


        $this->image = null;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getMetaTitle(): string
    {
        return $this->metaTitle;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): string
    {
        return $this->metaDescription;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle(string $subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @param string $metaTitle
     */
    public function setMetaTitle(string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription(string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    public function getSlug()
    {
        return StringUtil::cleanString($this->title);
    }




}