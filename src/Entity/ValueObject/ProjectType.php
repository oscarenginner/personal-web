<?php
/**
 * User: Oscar Sanchez
 * Date: 5/8/20
 */

namespace App\Entity\ValueObject;


use http\Exception\InvalidArgumentException;

class ProjectType
{
    const ACADEMIC = 'academic';
    const JOB = 'job';
    const PERSONAL = 'personal';

    const VALID_ROLES = [
        self::ACADEMIC,
        self::JOB,
        self::PERSONAL
    ];

    const VALUES_FOR_SELECT = [
        self::ACADEMIC => 'project.type.academic',
        self::JOB => 'project.type.job',
        self::PERSONAL => 'project.type.personal'
    ];

    /**
     * @var string
     */
    private $type;

    public function __construct(string $type)
    {
        $this->add($type);
    }

    private function add(string $type): void
    {
        if (!$this->isValid($type)) {
            throw new InvalidArgumentException();
        }

        $this->type = $type;

    }

    /**
     * @param string
     *
     * @return bool
     */
    public function isValid(string $type): bool
    {
        return in_array($type, self::VALID_ROLES);
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->type;
    }

}