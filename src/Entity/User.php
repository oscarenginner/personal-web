<?php

namespace App\Entity;

use App\Entity\ValueObject\PasswordEncoded;
use App\Entity\ValueObject\Roles;
use App\Image\ImageManipulator;
use App\Util\UploadsUtil;

class User
{
    const UPLOAD_SUBDIRECTORY = 'avatar';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $email;

    /**
     * @var PasswordEncoded;
     */
    private $password;

    /**
     * @var Roles
     */
    private $roles;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $surname;

    /**
     * @var string
     */
    private $instagramLink;

    /**
     * @var string
     */
    private $facebookLink;

    /**
     * @var string
     */
    private $githubLink;

    /**
     * @var string
     */
    private $hackerRankLink;

    /**
     * @var string
     */
    private $linkedinLink;

    /**
     * @var string
     */
    private $codepenLink;

    /**
     * @var string
     */
    private $whatsappLink;

    /**
     * @var string
     */
    private $shortDescription;

    /**
     * @var string
     */
    private $history;

    /**
     * @var string
     */
    private $avatar;

    public function __construct(string $username, string $email, PasswordEncoded $password)
    {
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->roles = new Roles(Roles::VALID_ROLES);
    }

    public function addAvatar(string $file)
    {
        if (isset($this->avatar)) {
            $this->deleteAvatar();
        }

        $fileName = uniqid().'.';
        $extension = ImageManipulator::getExtensionTmpFile($file);
        $fileOutput = UploadsUtil::getDirectoryPrivateUploadsPath(self::UPLOAD_SUBDIRECTORY).'/'.$fileName;

        ImageManipulator::generateMiniature($fileOutput,$file);

        $this->avatar = UploadsUtil::getPublicPath(self::UPLOAD_SUBDIRECTORY).'/'.$fileName.$extension;
    }

    public function deleteAvatar() : void
    {
        try{
            UploadsUtil::delete(UploadsUtil::getDirectoryPath($this->avatar));

        }catch(\InvalidArgumentException $e) {
        }


        $this->avatar = null;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getInstagramLink(): string
    {
        return $this->instagramLink;
    }

    /**
     * @param string $instagramLink
     */
    public function setInstagramLink(string $instagramLink): void
    {
        $this->instagramLink = $instagramLink;
    }

    /**
     * @return string
     */
    public function getFacebookLink(): string
    {
        return $this->facebookLink;
    }

    /**
     * @param string $facebookLink
     */
    public function setFacebookLink(string $facebookLink): void
    {
        $this->facebookLink = $facebookLink;
    }

    public function getPassword():PasswordEncoded
    {
        return $this->password;
    }

    /**
     * @return Roles
     */
    public function getRoles(): Roles
    {
        return $this->roles;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getGithubLink(): string
    {
        return $this->githubLink;
    }

    /**
     * @param string $githubLink
     */
    public function setGithubLink(string $githubLink): void
    {
        $this->githubLink = $githubLink;
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     */
    public function setShortDescription(string $shortDescription): void
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * @return string|null
     */
    public function getHistory(): ?string
    {
        return $this->history;
    }

    /**
     * @param string $history
     */
    public function setHistory(string $history): void
    {
        $this->history = $history;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * @return string
     */
    public function getHackerRankLink(): string
    {
        return $this->hackerRankLink;
    }

    /**
     * @param string $hackerRankLink
     */
    public function setHackerRankLink(string $hackerRankLink): void
    {
        $this->hackerRankLink = $hackerRankLink;
    }

    /**
     * @return string
     */
    public function getLinkedinLink(): string
    {
        return $this->linkedinLink;
    }

    /**
     * @param string $linkedinLink
     */
    public function setLinkedinLink(string $linkedinLink): void
    {
        $this->linkedinLink = $linkedinLink;
    }

    /**
     * @return string
     */
    public function getCodepenLink(): string
    {
        return $this->codepenLink;
    }

    /**
     * @param string $codepenLink
     */
    public function setCodepenLink(string $codepenLink): void
    {
        $this->codepenLink = $codepenLink;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getWhatsappLink(): string
    {
        return $this->whatsappLink;
    }

    /**
     * @param string $whatsappLink
     */
    public function setWhatsappLink(string $whatsappLink): void
    {
        $this->whatsappLink = $whatsappLink;
    }


}