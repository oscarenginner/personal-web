<?php
/**
 * User: Oscar Sanchez
 * Date: 6/8/20
 */

namespace App\Entity;


use App\Image\ImageManipulator;
use App\Util\StringUtil;
use App\Util\UploadsUtil;
use Symfony\Component\HttpFoundation\File\File;

class ProjectImage
{
    const UPLOAD_SUBDIRECTORY = 'project';
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var string|null
     */
    private $thumbnail;

    /**
     * @var string|null
     */
    private $fullCover;

    /**
     * @var string
     */
    private $path;

    /**
     * @var Project
     */
    private $project;

    /**
     * ProjectImage constructor.
     * @param string $fileName
     * @param string $path
     * @param Project $project
     */
    private function __construct(string $fileName, string $path, Project $project)
    {
        $this->fileName = $fileName;
        $this->path = $path;
        $this->project = $project;
    }

    /**
     * @param Project $project
     * @param File $file
     * @return ProjectImage
     */
    public static function createAndUploadFromProject(Project $project, File $file) : ProjectImage
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $projectImage = new self($fileName, self::UPLOAD_SUBDIRECTORY.'/'.StringUtil::cleanString($project->getName()),$project);
        $projectImage->upload($file);

        return $projectImage;

    }

    /**
     * @param File $file
     */
    public function upload(File $file) : void
    {
        UploadsUtil::createSubdirectoryIfNotExists(self::UPLOAD_SUBDIRECTORY);
        UploadsUtil::upload($file, $this->path, $this->fileName);
        $this->generateThumbnail();
        $this->generateFullCover();

    }

    /**
     * @return false
     */
    public function generateThumbnail()
    {
        if($this->thumbnail) {
            $this->deleteThumbnail();
        }

        if(!$this->fileName)
            return false;

        $imagePath = $this->path.'/'.$this->fileName;
        $fileNameThumbnail = StringUtil::cleanString($this->project->getName()).'-thumb-'.uniqid().'.';
        $extension =  ImageManipulator::getExtensionTmpFile(UploadsUtil::getDirectoryPathUploads($imagePath));
        $target = $this->path.'/'.$fileNameThumbnail;
        $target = UploadsUtil::getDirectoryPathUploads($target);
        ImageManipulator::generateThumbnails($target,UploadsUtil::getDirectoryPathUploads($imagePath));
        $this->thumbnail = $fileNameThumbnail.$extension;

    }

    /**
     * @return false
     */
    public function generateFullCover()
    {
        if($this->thumbnail) {
            $this->deleteFullCover();
        }

        if(!$this->fileName)
            return false;

        $imagePath = $this->path.'/'.$this->fileName;
        $fullCoverImage = StringUtil::cleanString($this->project->getName()).'-full-'.uniqid().'.';
        $extension =  ImageManipulator::getExtensionTmpFile(UploadsUtil::getDirectoryPathUploads($imagePath));
        $target = $this->path.'/'.$fullCoverImage;
        $target = UploadsUtil::getDirectoryPathUploads($target);
        ImageManipulator::generateFullCover($target,UploadsUtil::getDirectoryPathUploads($imagePath));
        $this->fullCover = $fullCoverImage.$extension;

    }

    public function deleteMedias()
    {
        $this->deleteImageFile();
        $this->deleteFullCover();
        $this->deleteThumbnail();
    }

    /**
     *
     */
    private function deleteImageFile(): void
    {
        if (empty($this->fileName)) {
            return;
        }

        try{
            UploadsUtil::delete($this->path.'/'.$this->fileName);

        }catch(\InvalidArgumentException $e) {

        }

        $this->fileName = '';
    }

    /**
     *
     */
    private function deleteThumbnail(): void
    {
        if (empty($this->thumbnail)) {
            return;
        }

        try{
            UploadsUtil::delete($this->path.'/'.$this->thumbnail);

        }catch(\InvalidArgumentException $e) {

        }

        $this->thumbnail = '';
    }

    /**
     *
     */
    private function deleteFullCover() : void
    {
        if (empty($this->fullCover)) {
            return;
        }

        try{
            UploadsUtil::delete($this->path.'/'.$this->fullCover);

        }catch(\InvalidArgumentException $e) {

        }

        $this->fullCover = '';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getThumbnail()
    {
        return UploadsUtil::getPublicPath($this->path.'/'.$this->thumbnail);
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

}