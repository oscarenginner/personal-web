<?php
/**
 * User: Oscar Sanchez
 * Date: 30/7/20
 */

namespace App\Entity;


use App\Image\ImageManipulator;
use App\Util\FileUtil;
use App\Util\UploadsUtil;
use Symfony\Component\HttpFoundation\File\File;

class SkillCategory
{
    const ICON_FOLDER = 'skillcategory';
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $icon;

    /**
     * SkillCategory constructor.
     * @param string $name
     * @param string $icon
     */
    public function __construct(string $name, File $icon)
    {
        $this->name = $name;
        $this->addIcon($icon);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        if(!$this->icon) {
            return null;
        }

        $path = UploadsUtil::getDirectoryPathUploads(self::ICON_FOLDER);
        $iconFile = $path.'/'.$this->icon;

        if(!file_exists($iconFile)) {
            return null;
        }
        return FileUtil::getContentFile($iconFile);
    }

    /**
     * @param File $file
     */
    public function addIcon(File $file) : void
    {
        $fileName = $this->name.'-'.uniqid().'.';
        $extension ='svg';

        UploadsUtil::upload($file,self::ICON_FOLDER,$fileName.$extension);

        $this->icon = $fileName.$extension;
    }


    /**
     *
     */
    public function deleteIcon(): void
    {
        try{
            $path = UploadsUtil::getDirectoryPathUploads(self::ICON_FOLDER);
            $iconFile = $path.'/'.$this->icon;

            if(file_exists($iconFile)) {
              UploadsUtil::delete($iconFile);
            }

        }catch(\InvalidArgumentException $e) {

        }

        $this->icon = null;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}