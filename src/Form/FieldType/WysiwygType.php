<?php

namespace App\Form\FieldType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WysiwygType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'empty_data' => '',
            'label' => false,
            'attr' => [
                'class' => 'js-summernote',
            ],
            'sanitize_html' => true,
        ]);
    }

    /**
     * @return string|null
     */
    public function getParent()
    {
        return TextareaType::class;
    }
}
