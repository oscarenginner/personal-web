<?php
/**
 * User: Oscar Sanchez
 * Date: 13/4/20
 */

namespace App\Form\FieldType;

use App\Entity\ValueObject\ProjectType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectTypeChoiceType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'placeholder' => false,
            'choices' => array_flip(ProjectType::VALUES_FOR_SELECT)
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}