<?php
/**
 * User: Oscar Sanchez
 * Date: 4/8/20
 */

namespace App\Form\Type;


use App\Form\FieldType\ProjectTypeChoiceType;
use App\Form\FieldType\WysiwygType;
use App\Repository\SkillRepositoryInterface;
use App\Service\DTO\ProjectUpdateDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectUpdateType extends AbstractType
{
    /**
     * @var SkillRepositoryInterface
     */
    private $skillRepository;

    /**
     * ExperienceCreateType constructor.
     * @param SkillRepositoryInterface $skillRepository
     */
    public function __construct(SkillRepositoryInterface $skillRepository)
    {
        $this->skillRepository = $skillRepository;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'project.form.name',
                'empty_data' => '',

            ])->add('type', ProjectTypeChoiceType::class, [
                'empty_data' => '',
            ])
            ->add('description', WysiwygType::class, [
                'label' => 'user.update.description',
                'empty_data' => ''

            ])->add('shortDescription', TextareaType::class, [
                'label' => 'user.update.shortDescription',
                'empty_data' => '',
                'required' => false,

            ])->add('url', UrlType::class, [
                'label' => 'project.form.url',
                'empty_data' => '',
                'required' => false

            ])->add('logo', FileType::class, [
                'label' => 'project.form.logo',
                'required' => false

            ])->add('imageFull', FileType::class, [
                'label' => 'project.form.fullImage',
                'required' => false

             ])->add('imageList', FileType::class, [
                'label' => 'project.form.imageList',
                'required' => false

            ])->add('imageListPhone', FileType::class, [
                'label' => 'project.form.imageList',
                'required' => false

            ])->add('date', DateType::class, [
                'label' => 'project.form.date',
                'required' => false,
                'years' => range(date('Y')-50, date('Y')),
                'placeholder' => [
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                ]

            ])->add('skills', ChoiceType::class, [
                'label' => 'experience.form.jobDescription',
                'multiple' => true,
                'empty_data' => [],
                'required' => false,
                'choices' => $this->skillRepository->getAllAsKeyValueArray(),

            ])
            ->add('file', FileType::class, [
                'label' => 'project.form.file',
                'required' => false

            ])->add('buttonFileText', TextType::class, [
                'label' => 'project.form.button.text',
                'required' => false,
                'empty_data' => '',

            ])->add('images', FileType::class, [
                'label' => 'project.form.images',
                'required' => false,
                'multiple' => true
            ])->add('featured', ChoiceType::class, [
                'label' => 'post.form.featured',
                'empty_data' => false,
                'required' => true,
                'choices' => [
                    'Yes' => true,
                    'No' => false
                ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProjectUpdateDTO::class]);
    }
}