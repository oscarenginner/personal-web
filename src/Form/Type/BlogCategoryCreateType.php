<?php
/**
 * User: Oscar Sanchez
 * Date: 19/8/20
 */

namespace App\Form\Type;


use App\Form\FieldType\WysiwygType;
use App\Service\DTO\BlogCategoryCreateDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogCategoryCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'experience.form.company',
                'empty_data' => '',

            ])->add('image', FileType::class, [
                'label' => 'experience.form.image',
                'required' => false

            ])
            ->add('subtitle', TextType::class, [
                'label' => 'experience.form.subtitle',
                'empty_data' => '',

            ])->add('content', WysiwygType::class, [
                'label' => 'experience.form.content',
                'empty_data' => ''
            ])->add('metaTitle', TextType::class, [
                'label' => 'experience.form.metaTitle',
                'empty_data' => '',

            ])->add('metaDescription', TextareaType::class, [
                'label' => 'experience.form.metaTitle',
                'empty_data' => '',

            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BlogCategoryCreateDTO::class]);
    }

}