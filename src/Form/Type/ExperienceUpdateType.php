<?php
/**
 * User: Oscar Sanchez
 * Date: 4/8/20
 */

namespace App\Form\Type;


use App\Form\FieldType\WysiwygType;
use App\Repository\SkillRepositoryInterface;
use App\Service\DTO\ExperienceCreateDTO;
use App\Service\DTO\ExperienceUpdateDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExperienceUpdateType extends AbstractType
{
    /**
     * @var SkillRepositoryInterface
     */
    private $skillRepository;

    /**
     * ExperienceCreateType constructor.
     * @param SkillRepositoryInterface $skillRepository
     */
    public function __construct(SkillRepositoryInterface $skillRepository)
    {
        $this->skillRepository = $skillRepository;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company', TextType::class, [
                'label' => 'experience.form.company',
                'empty_data' => '',

            ])->add('image', FileType::class, [
                'label' => 'experience.form.image',
                'required' => false

            ])->add('initDate', DateType::class, [
                'label' => 'experience.form.initDate',
                'years' => range(date('Y')-50, date('Y')),

            ])->add('endDate', DateType::class, [
                'label' => 'experience.form.endDate',
                'required' => false,
                'years' => range(date('Y')-50, date('Y')),
                'placeholder' => [
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                ]

            ])->add('jobPosition', TextType::class, [
                'label' => 'experience.form.jobPosition',
                'empty_data' => '',

            ])->add('jobDescription', WysiwygType::class, [
                'label' => 'experience.form.jobDescription',
                'empty_data' => ''
            ])->add('skills', ChoiceType::class, [
                'label' => 'experience.form.jobDescription',
                'multiple' => true,
                'required' => false,
                'choices' => $this->skillRepository->getAllAsKeyValueArray(),
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExperienceUpdateDTO::class]);
    }
}