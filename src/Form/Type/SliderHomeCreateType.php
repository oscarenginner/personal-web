<?php
/**
 * User: Oscar Sanchez
 * Date: 21/8/20
 */

namespace App\Form\Type;

use App\Service\DTO\SliderHomeCreateDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SliderHomeCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'slider_home.form.title',
                'empty_data' => '',

            ])->add('subtitle', TextType::class, [
                'label' => 'slider_home.form.subtitle',
                'empty_data' => '',

            ])->add('image', FileType::class, [
                'label' => 'slider_home.form.image',
                'required' => false

            ])->add('imagePhone', FileType::class, [
                'label' => 'slider_home.form.imagePhone',
                'required' => false

            ])->add('videoYoutubeLink', TextType::class, [
                'label' => 'slider_home.form.youtube_link',
                'empty_data' => '',
                'required' => false

            ])->add('link', TextType::class, [
                'label' => 'slider_home.form.link',
                'empty_data' => '',
                'required' => false
                ,
            ])->add('buttonText', TextType::class, [
                'label' => 'slider_home.form.text_button',
                'empty_data' => '',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SliderHomeCreateDTO::class]);
    }

}