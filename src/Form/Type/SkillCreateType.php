<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Form\Type;


use App\Repository\SkillCategoryRepositoryInterface;
use App\Service\DTO\SkillCreateDTO;
use App\Service\DTO\SkillUpdateDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SkillCreateType extends AbstractType
{

    /**
     * @var SkillCategoryRepositoryInterface
     */
  private $skillCategoryRepository;

    /**
     * SkillCreateType constructor.
     * @param SkillCategoryRepositoryInterface $skillCategoryRepository
     */
    public function __construct(SkillCategoryRepositoryInterface $skillCategoryRepository)
    {
        $this->skillCategoryRepository = $skillCategoryRepository;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
          ->add('name', TextType::class, [
              'label' => 'skill.form.name',
              'empty_data' => '',

          ])->add('score', IntegerType::class, [
              'label' => 'skill.form.score',
              'required' => true
          ])->add('skillCategory',ChoiceType::class, [
              'choices' => $this->skillCategoryRepository->getAllAsKeyValueArray(),
          ])->add('featured', ChoiceType::class, [
              'label' => 'skill.form.is.featured',
              'empty_data' => false,
              'required' => true,
              'choices' => [
                  'Yes' => true,
                  'No' => false
              ]
          ])->add('image', FileType::class, [
              'label' => 'skill.create.image',
              'required' => false
          ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SkillCreateDTO::class]);
    }
}