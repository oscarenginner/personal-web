<?php
/**
 * User: Oscar Sanchez
 * Date: 30/7/20
 */

namespace App\Form\Type;

use App\Service\DTO\SkillCategoryCreateDTO;
use App\Service\DTO\SkillCategoryUpdateDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SkillCategoryUpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'skill.category.form.name',
                'empty_data' => '',

            ])->add('icon', FileType::class, [
                'label' => 'skill.category.form.icon',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SkillCategoryUpdateDTO::class]);
    }
}