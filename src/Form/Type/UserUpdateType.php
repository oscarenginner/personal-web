<?php

namespace App\Form\Type;

use App\Form\FieldType\WysiwygType;
use App\Service\DTO\UserUpdateDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use function Sodium\add;

class UserUpdateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'user.update.name',
                'empty_data' => '',
            ])
            ->add('surname', TextType::class, [
                'label' => 'user.update.surname',
                'empty_data' => '',
            ])
            ->add('phone', TextType::class, [
                'label' => 'user.update.phone',
                'empty_data' => '',
            ])
            ->add('instagramLink', TextType::class, [
                'label' => 'user.update.instagram',
                'empty_data' => '',
            ])
            ->add('facebookLink', TextType::class, [
                'label' => 'user.update.facebook',
                'empty_data' => '',
            ])
            ->add('githubLink', TextType::class, [
                'label' => 'user.update.github',
                'empty_data' => '',
            ])
            ->add('hackerRankLink', TextType::class, [
                'label' => 'user.update.hackerrank',
                'empty_data' => '',
            ])
            ->add('linkedinLink', TextType::class, [
                'label' => 'user.update.likedinLink',
                'empty_data' => '',
            ])
            ->add('codepenLink', TextType::class, [
                'label' => 'user.update.codepen',
                'empty_data' => '',
            ])
            ->add('whatsappLink', TextType::class, [
                'label' => 'user.update.whatsapp',
                'empty_data' => '',
            ])
            ->add('shortDescription', TextareaType::class, [
                'label' => 'user.update.short.description',
                'empty_data' => '',
            ])
            ->add('history', WysiwygType::class, [
                'label' => 'user.update.history',
                'empty_data' => ''
            ])
            ->add('avatar', FileType::class, [
                'label' => 'user.update.avatar',
                'required' => false
            ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserUpdateDTO::class]);
    }

}