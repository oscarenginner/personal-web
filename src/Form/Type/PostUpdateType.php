<?php
/**
 * User: Oscar Sanchez
 * Date: 20/8/20
 */

namespace App\Form\Type;


use App\Form\FieldType\WysiwygType;
use App\Repository\BlogCategoryRepositoryInterface;
use App\Service\DTO\PostCreateDTO;
use App\Service\DTO\PostUpdateDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostUpdateType extends AbstractType
{
    /**
     * @var BlogCategoryRepositoryInterface
     */
    private $blogCategoryRepository;

    /**
     * PostCategoryType constructor.
     * @param BlogCategoryRepositoryInterface $blogCategoryRepository
     */
    public function __construct(BlogCategoryRepositoryInterface $blogCategoryRepository)
    {
        $this->blogCategoryRepository = $blogCategoryRepository;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'post.form.title',
                'empty_data' => '',

            ])->add('subtitle', TextType::class, [
                'label' => 'post.form.subtitle',
                'empty_data' => '',

            ])->add('content', WysiwygType::class, [
                'label' => 'post.form.content',
                'empty_data' => ''
            ])->add('image', FileType::class, [
                'label' => 'post.form.image',
                'required' => false

            ])->add('imageList', FileType::class, [
                'label' => 'post.form.imageList',
                'required' => false

            ])->add('category', ChoiceType::class, [
                'label' => 'post.form.category',
                'multiple' => false,
                'empty_data' => '',
                'choices' => $this->blogCategoryRepository->getAllAsKeyValueArray(),
            ])->add('excerpt', TextareaType::class, [
                'label' => 'post.form.excerpt',
                'empty_data' => '',
            ])->add('metaTitle', TextType::class, [
                'label' => 'post.form.metaTitle',
                'empty_data' => '',
            ])->add('metaDescription', TextareaType::class, [
                'label' => 'post.form.excerpt',
                'empty_data' => '',
            ])->add('publish', ChoiceType::class, [
                'label' => 'post.form.publish',
                'empty_data' => false,
                'required' => true,
                'choices' => [
                    'Yes' => true,
                    'No' => false
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PostUpdateDTO::class]);
    }
}