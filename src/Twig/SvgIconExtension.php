<?php

namespace App\Twig;

use Symfony\Component\Asset\Packages;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SvgIconExtension extends AbstractExtension
{
    private const PUBLIC_ICONS_DIRECTORY = 'icons';

    /**
     * @var Packages
     */
    private $packages;

    /**
     * SvgIconExtension constructor.
     *
     * @param Packages $packages
     */

    /**
     * @var string
     */
    private $appBasePath;

    public function __construct(Packages $packages, string $appBasePath)
    {
        $this->packages = $packages;
        $this->appBasePath = $appBasePath;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('icon', [$this, 'renderIcon'], [
                'is_safe' => ['html'],
            ]),
        ];
    }

    public function renderIcon(string $iconName, int $size = 32, string $className = ''): string
    {
        $icon = $iconName.'.svg';
        $iconPath = $this->appBasePath.'/'.self::PUBLIC_ICONS_DIRECTORY.'/'.$icon;
        $iconPath = $this->packages->getUrl($iconPath);

        $classNames = 'icon icon-'.$iconName;

        $test = file_exists($iconPath);

        $test2  = file_exists($this->appBasePath.'house.svg');
        if (empty($iconPath)) {
            return 'ICON NOT FOUND';
        }

        if (!empty($className)) {
            $classNames .= ' '.$className;
        }

        $content = file_get_contents($iconPath);

        if (32 !== $size && $size > 0) {
            $height = sprintf('height="%d"', $size);
            $width = sprintf('width="%d"', $size);
            $content = str_replace('height="32"', $height, $content);
            $content = str_replace('width="32"', $width, $content);
        }

        $content = preg_replace('/\r|\n|/', '', $content);
        $content = preg_replace('/<!--(.*)-->/Uis', '', $content);
        $svgWithClassNames = sprintf('<svg class="%s"', $classNames);
        $content = str_replace('<svg', $svgWithClassNames, $content);

        return $content;
    }
}
