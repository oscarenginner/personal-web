<?php
/**
 * User: Oscar Sanchez
 * Date: 4/8/20
 */

namespace App\Util;


use DateTimeInterface;

class DateUtil
{
    const SPA_LOCALE = 'es_ES.utf8';

    const ALL_FORMAT = 'Y-m-d H:i:s';

    const MOUNT_TEXT_YEAR_COMPLETE_FORMAT = '%B %G';

    /**
     * @param DateTimeInterface $dateTime
     * @param string $format
     * @param string $locale
     */
    public static function getDateFormatLangFromDateTime(DateTimeInterface $dateTime,
                                                         string $format = self::MOUNT_TEXT_YEAR_COMPLETE_FORMAT,
                                                         string $locale = self::SPA_LOCALE)
    {
        setlocale(LC_ALL, $locale);

        return strftime( $format,strtotime($dateTime->format(self::ALL_FORMAT)));
    }

    /**
     * @param DateTimeInterface $dateTimeInit
     * @param DateTimeInterface $dateTimeEnd
     * @param bool $rounded
     * @param bool $showDays
     * @param string $locale
     * @return string
     */
    public static function dateDiff(DateTimeInterface $dateTimeInit,
                             DateTimeInterface $dateTimeEnd,
                             bool $rounded = true,
                             bool $showDays = false,
                                    string $locale = self::SPA_LOCALE)
    {
        $interval = $dateTimeInit->diff($dateTimeEnd);

        $days = $interval->d;
        $mouth = $interval->m;
        $year = $interval->y;

        if($rounded) {
            if($days > 15) {
                $mouth++;
                $days = 0;
            }

            if($mouth == 12) {
                $year++;
                $mouth = 0;
            }
        }

        $diff = '';

        if($year > 0) {
            $diff .= $year.' Año ';
        }

        if($mouth > 0) {
            $diff .= $mouth. ' Meses';
        }

        if($showDays && $days > 0) {
            $diff .= $days.' Días';
        }

        return $diff;
    }

    /**
     * @param DateTimeInterface $date
     * @param string $format
     * @return string
     */
    public static function getDateFormatByDateTime(DateTimeInterface $date, string $format = self::ALL_FORMAT) : string
    {
        return $date->format($format);
    }
}