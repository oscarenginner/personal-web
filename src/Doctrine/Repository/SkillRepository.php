<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Doctrine\Repository;


use App\Entity\Skill;
use App\Repository\SkillRepositoryInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Exception\SkillNotFoundException;

class SkillRepository extends EntityRepository implements SkillRepositoryInterface
{
    /**
     * @return array
     */
    public function getAll(): array
    {
        return parent::findAll();
    }

    /**
     * @param Skill $skill
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Skill $skill): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($skill);
        $entityManager->flush();
    }

    /**
     * @param int $id
     * @return Skill
     * @throws SkillNotFoundException
     */
    public function getById(int $id): Skill
    {
        /** @var Skill $skill */
        $skill =  $this->find($id);

        if(!$skill) {
            throw new SkillNotFoundException();
        }

        return $skill;
    }

    /**
     * @param Skill $skill
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Skill $skill): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($skill);
        $entityManager->flush();
    }

    public function findByName(string $name): ?Skill
    {
        return $this->findOneBy(['name' => $name]);
    }

    /**
     * @return array
     */
    public function getAllAsKeyValueArray(): array
    {
        $skills = $this->getAll();

        $skillsArray = [];

        /** @var Skill $skill */
        foreach ($skills as $skill) {
            $skillsArray[$skill->getName()] = $skill->getId();
        }

        return $skillsArray;
    }

    public function getAllFeatured(): array
    {
        return $this->findBy(['featured' => 1]);
    }
}