<?php
/**
 * User: Oscar Sanchez
 * Date: 20/8/20
 */

namespace App\Doctrine\Repository;


use App\Entity\Post;
use App\Exception\PostNotFoundException;
use App\Repository\PostRepositoryInterface;
use App\Service\DTO\BlogQueryDTO;
use App\Service\Query\PagedQueryResult;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;

class PostRepository extends EntityRepository implements PostRepositoryInterface
{

    /**
     * @param Post $post
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Post $post): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($post);
        $entityManager->flush();
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return parent::findAll();
    }

    /**
     * @param int $id
     * @return Post
     * @throws PostNotFoundException
     */
    public function getById(int $id): Post
    {
        /** @var Post $post */
        $post = $this->find($id);

        if(!$post) {
            throw new PostNotFoundException();
        }

        return $post;

    }

    /**
     * @param Post $post
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Post $post): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($post);
        $entityManager->flush();
    }

    public function getLastPublish(int $limit): array
    {
        return $this->findBy(['isPublished' => 1],['publishedAt' => 'DESC'],$limit);
    }

    public function findByQuery(BlogQueryDTO $queryDTO): PagedQueryResult
    {
        $paged = $queryDTO->getPagedQuery();
        $qb = $this->createQueryBuilderForPagination();

        $query = $qb->getQuery();
        $query->setFirstResult($paged->offset());
        $query->setMaxResults($paged->limit() ?: null);

        $result = $query->getResult();

        return new PagedQueryResult(
            $paged->page(),
            $paged->limit(),
            $this->countByQueryBuilder($qb),
            $result
        );

    }

    /**
     * @param QueryBuilder $qb
     *
     * @return int
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByQueryBuilder(QueryBuilder $qb): int
    {
        $qb->select('count(p)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }


    public function getDistinctCategories(): array
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('identity(p.category)');
        $qb->distinct();

        $query = $qb->getQuery();

        return $query->getResult();

    }


    private function createQueryBuilderForPagination(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('p');

            $qb->where('p.isPublished = 1');
            $qb->orderBy('p.publishedAt', 'DESC');


        return $qb;
    }
}