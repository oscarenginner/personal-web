<?php
/**
 * User: Oscar Sanchez
 * Date: 30/7/20
 */

namespace App\Doctrine\Repository;

use App\Entity\SkillCategory;
use App\Repository\SkillCategoryRepositoryInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use App\Exception\SkillCategoryNotFoundException;

class SkillCategoryRepository extends EntityRepository implements SkillCategoryRepositoryInterface
{
    /**
     * @return array
     */
    public function getAll(): array
    {
        return parent::findAll();
    }


    /**
     * @param SkillCategory $skillCategory
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(SkillCategory $skillCategory): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($skillCategory);
        $entityManager->flush();
    }

    /**
     * @param int $id
     * @return SkillCategory
     * @throws SkillCategoryNotFoundException
     */
    public function getById(int $id): SkillCategory
    {
        /** @var SkillCategory $skillCategory */
        $skillCategory =  $this->find($id);

        if(!$skillCategory) {
            throw new SkillCategoryNotFoundException();
        }

        return $skillCategory;
    }

    /**
     * @param SkillCategory $skillCategory
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(SkillCategory $skillCategory): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($skillCategory);
        $entityManager->flush();
    }


    /**
     * @param $name
     * @return SkillCategory|null
     */
    public function findByName($name): ?SkillCategory
    {
        return $this->findOneBy(['name' => $name]);
    }

    public function getAllAsKeyValueArray(): array
    {
        $skillCategories = $this->getAll();

        $skillCategoriesArray = [];

        /** @var SkillCategory $skillCategory */
        foreach ($skillCategories as $skillCategory) {
            $skillCategoriesArray[$skillCategory->getName()] = $skillCategory->getId();
        }

        return $skillCategoriesArray;
    }
}