<?php

namespace App\Doctrine\Repository;

use App\Entity\User;
use App\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserRepository extends EntityRepository implements UserRepositoryInterface
{

    public function getAll(): array
    {
        return parent::findAll();
    }

    /**
     * @param string $username
     *
     * @return User|null
     */
    public function findByUsername(string $username): ?User
    {
        return $this->findOneBy(['username' => $username]);
    }

    /**
     * @param User $user
     *
     * @throws ORMException
     */
    public function save(User $user): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($user);
        $entityManager->flush();
    }

    /**
     * @param int $id
     * @return User
     */
    public function getById(int $id): User
    {
        /** @var User $user */
        $user =  $this->find($id);

        if(!$user) {
            throw new UsernameNotFoundException();
        }

        return $user;
    }
}