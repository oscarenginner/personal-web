<?php
/**
 * User: Oscar Sanchez
 * Date: 19/8/20
 */

namespace App\Doctrine\Repository;


use App\Entity\BlogCategory;
use App\Exception\BlogCategoryExceptionNotFound;
use App\Repository\BlogCategoryRepositoryInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class BlogCategoryRepository extends EntityRepository implements BlogCategoryRepositoryInterface
{

    /**
     * @param BlogCategory $blogCategory
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(BlogCategory $blogCategory): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($blogCategory);
        $entityManager->flush();
    }

    public function getAll(): array
    {
        return parent::findAll();
    }

    /**
     * @param int $id
     * @return BlogCategory
     * @throws BlogCategoryExceptionNotFound
     */
    public function getById(int $id): BlogCategory
    {
        /** @var BlogCategory $blogCategory */
        $blogCategory = $this->find($id);

        if(!$blogCategory) {
            throw new BlogCategoryExceptionNotFound();
        }

        return $blogCategory;

    }

    /**
     * @param BlogCategory $blogCategory
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(BlogCategory $blogCategory): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($blogCategory);
        $entityManager->flush();
    }

    public function getAllAsKeyValueArray(): array
    {
        $blogCategories = $this->getAll();

        $blogCategoriesArray = [];

        /** @var BlogCategory $blogCategory */
        foreach ($blogCategories as $blogCategory) {
            $blogCategoriesArray[$blogCategory->getTitle()] = $blogCategory->getId();
        }

        return $blogCategoriesArray;
    }
}