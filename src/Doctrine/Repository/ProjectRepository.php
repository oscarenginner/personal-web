<?php
/**
 * User: Oscar Sanchez
 * Date: 5/8/20
 */

namespace App\Doctrine\Repository;


use App\Entity\Project;
use App\Entity\ProjectImage;
use App\Exception\ProjectNotFoundException;
use App\Repository\ProjectRepositoryInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;

class ProjectRepository extends EntityRepository implements ProjectRepositoryInterface
{
    /**
     * @param Project $project
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Project $project): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($project);
        $entityManager->flush();
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return parent::findAll();
    }

    /**
     * @return array
     */
    public function getAllOrderByDate(): array
    {
        return $this->findBy(array(), array('date' => 'DESC'));
    }

    /**
     * @param int $id
     * @return Project
     * @throws ProjectNotFoundException
     */
    public function getById(int $id): Project
    {
        /** @var Project $project */
        $project = $this->find($id);

        if(!$project) {
            throw new ProjectNotFoundException();
        }

        return $project;
    }

    /**
     * @param Project $project
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Project $project): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($project);
        $entityManager->flush();
    }

    /**
     * @param int $imageId
     * @return Project
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ProjectNotFoundException
     * @throws TransactionRequiredException
     */
    public function getByImageId(int $imageId): Project
    {
        $conn = $this->getEntityManager();
        /** @var ProjectImage $projectImage */
        $projectImage = $conn->find(ProjectImage::class,$imageId);

        if(!isset($projectImage)) {
            throw new ProjectNotFoundException();
        }
        $project = $projectImage->getProject();

        return $project;
    }

    /**
     * @return array
     */
    public function getFeatured(): array
    {
        return $this->findBy(['featured' => 1]);
    }
}