<?php
/**
 * User: Oscar Sanchez
 * Date: 21/8/20
 */

namespace App\Doctrine\Repository;


use App\Entity\SliderHome;
use App\Exception\SliderHomeNotFoundException;
use App\Repository\SliderHomeRepositoryInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class SliderHomeRepository extends EntityRepository implements SliderHomeRepositoryInterface
{

    /**
     * @return int
     */
    public function getPositionMax(): int
    {
      $query = $this->createQueryBuilder('s');
      $query->select('MAX(s.position) AS max_position');

      $result = $query->getQuery()->getSingleResult();

      if(!$result['max_position']) {
          return 0;
      }

      return $result['max_position'];
    }

    /**
     * @param SliderHome $sliderHome
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(SliderHome $sliderHome)
    {
       $entityManager = $this->getEntityManager();
       $entityManager->persist($sliderHome);
       $entityManager->flush();
    }

    public function getAll(): array
    {
        return parent::findAll();


    }

    /**
     * @return array
     */
    public function getAllOrderByPosition(): array
    {
        return $this->findBy([], ['position' => 'ASC']);
    }

    /**
     * @param int $id
     * @return SliderHome
     * @throws SliderHomeNotFoundException
     */
    public function getById(int $id): SliderHome
    {
        /** @var SliderHome $sliderHome */
        $sliderHome = $this->find($id);

        if(!$sliderHome) {
            throw new SliderHomeNotFoundException();
        }

        return $sliderHome;
    }

    /**
     * @param string $position
     * @return array
     */
    public function getUpPosition(string $position): array
    {
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->gt('position', $position));

        $result =  $this->matching($criteria);

        return $result->toArray();
    }

    public function getEqualsAndUpPositionRangeByTwoPositions(int $position, int $positionTwo): array
    {
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->gte('position', $position));
        $criteria->andWhere($criteria->expr()->lte('position', $positionTwo));

        $result =  $this->matching($criteria);

        return $result->toArray();
    }

    /**
     * @param SliderHome $slider
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(SliderHome $slider): void
    {
       $entityManager = $this->getEntityManager();
       $entityManager->remove($slider);
       $entityManager->flush();
    }

    public function getDownPositionRangeByTwoPositions(int $position, int $positionTwo): array
    {
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->lte('position', $position));
        $criteria->andWhere($criteria->expr()->gte('position', $positionTwo));

        $result =  $this->matching($criteria);

        return $result->toArray();
    }

    /**
     * @return SliderHome
     * @throws SliderHomeNotFoundException
     */
    public function getLast(): SliderHome
    {
        /** @var SliderHome $slider */
        $slider = $this->findOneBy([], ['id' => 'desc']);

        if(!$slider) {
            throw new SliderHomeNotFoundException();
        }

        return $slider;

    }
}