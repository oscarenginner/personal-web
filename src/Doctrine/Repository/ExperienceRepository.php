<?php
/**
 * User: Oscar Sanchez
 * Date: 4/8/20
 */

namespace App\Doctrine\Repository;


use App\Entity\Experience;
use App\Exception\ExperienceNotFoundException;
use App\Repository\ExperienceRepositoryInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class ExperienceRepository extends EntityRepository implements ExperienceRepositoryInterface
{

    /**
     * @return array
     */
    public function getAll(): array
    {
        return parent::findAll();
    }

    /**
     * @param Experience $experience
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Experience $experience): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($experience);
        $entityManager->flush();
    }

    public function getAllOrderByDateInit(): array
    {
        return $this->findBy(array(), array('initDate' => 'DESC'));
    }

    /**
     * @param int $id
     * @return Experience
     * @throws ExperienceNotFoundException
     */
    public function getById(int $id): Experience
    {
        /** @var Experience $experience */
        $experience =  $this->find($id);

        if(!$experience) {
            throw new ExperienceNotFoundException();
        }

        return $experience;
    }

    /**
     * @param Experience $experience
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(Experience $experience): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($experience);
        $entityManager->flush();
    }
}