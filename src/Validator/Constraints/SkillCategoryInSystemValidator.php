<?php


namespace App\Validator\Constraints;

use App\Repository\SkillCategoryRepositoryInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SkillCategoryInSystemValidator extends ConstraintValidator
{
    /**
     * @var SkillCategoryRepositoryInterface
     */
    private $skillCategoryRepository;


    public function __construct(SkillCategoryRepositoryInterface $skillCategoryRepository)
    {
        $this->skillCategoryRepository = $skillCategoryRepository;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof SkillCategoryInSystemConstraint) {
            throw new UnexpectedTypeException($constraint, SkillCategoryInSystemConstraint::class);
        }

        if($constraint->update) {

            $platformUpdateDTO = $this->context->getObject();

            $platform = $this->skillCategoryRepository->getById($platformUpdateDTO->getId());

            if($value != $platform->getName()) {
                if($this->skillCategoryRepository->findByName($value)) {
                    $this->context->buildViolation($constraint->message)
                        ->setParameter('{{ skillCategory }}', $value)
                        ->addViolation();
                }
            }

        }else{

            if($this->skillCategoryRepository->findByName($value)) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ skillCategory }}', $value)
                    ->addViolation();
            }

        }

    }
}