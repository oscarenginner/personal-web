<?php


namespace App\Validator\Constraints;

use App\Repository\SkillCategoryRepositoryInterface;
use App\Repository\SkillRepositoryInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SkillInSystemValidator extends ConstraintValidator
{
    /**
     * @var SkillRepositoryInterface
     */
    private $skillRepository;


    public function __construct(SkillRepositoryInterface $skillRepository)
    {
        $this->skillRepository = $skillRepository;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof SkillInSystemConstraint) {
            throw new UnexpectedTypeException($constraint, SkillInSystemConstraint::class);
        }

        if($constraint->update) {

            $platformUpdateDTO = $this->context->getObject();

            $platform = $this->skillRepository->getById($platformUpdateDTO->getId());

            if($value != $platform->getName()) {
                if($this->skillRepository->findByName($value)) {
                    $this->context->buildViolation($constraint->message)
                        ->setParameter('{{ skill }}', $value)
                        ->addViolation();
                }
            }

        }else{

            if($this->skillRepository->findByName($value)) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ skill }}', $value)
                    ->addViolation();
            }

        }

    }
}