<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class SkillInSystemConstraint extends Constraint
{
    public $message = ' La skill  {{ skill }} ya esta en el sistema';

    public $update = false;

    /**
     * EmailInSystemConstraint constructor.
     *
     * @param array $options
     */
    public function __construct(?array $options = null)
    {
        parent::__construct($options);
    }

    /**
     * @return string
     */
    public function validatedBy()
    {
        return SkillInSystemValidator::class;
    }

}