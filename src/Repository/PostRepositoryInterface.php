<?php
/**
 * User: Oscar Sanchez
 * Date: 20/8/20
 */

namespace App\Repository;


use App\Entity\Post;
use App\Service\DTO\BlogQueryDTO;
use App\Service\Query\PagedQueryResult;

interface PostRepositoryInterface
{

    public function save(Post $post) : void;

    public function getAll() : array;

    public function getById(int $id): Post;

    public function delete(Post $post) : void;

    public function getLastPublish(int $limit) : array;

    public function findByQuery(BlogQueryDTO $queryDTO): PagedQueryResult;

    public function getDistinctCategories(): array;
}