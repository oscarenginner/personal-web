<?php
/**
 * User: Oscar Sanchez
 * Date: 5/8/20
 */

namespace App\Repository;

use App\Entity\Project;

interface ProjectRepositoryInterface
{

    public function save(Project $project) : void;

    public function getAll() : array;

    public function getAllOrderByDate() : array;

    public function getById(int $id) : Project;

    public function delete(Project $project) : void;

    public function getByImageId(int $imageId) : Project;

    public function getFeatured() : array;
}