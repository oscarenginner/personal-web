<?php
/**
 * User: Oscar Sanchez
 * Date: 19/8/20
 */

namespace App\Repository;


use App\Entity\BlogCategory;

interface BlogCategoryRepositoryInterface
{
    public function save(BlogCategory $blogCategory) : void;

    public function getAll() : array;

    public function getById(int $id) : BlogCategory;

    public function delete(BlogCategory $blogCategory) : void;

    public function getAllAsKeyValueArray(): array;

}