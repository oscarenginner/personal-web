<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Repository;


use App\Entity\Skill;

interface SkillRepositoryInterface
{
    public function getAll() : array;

    public function save(Skill $skill): void;

    public function getById(int $id) : Skill;

    public function delete(Skill $skill): void;

    public function findByName(string $name): ?Skill;

    public function getAllAsKeyValueArray(): array;

    public function getAllFeatured(): array;

}