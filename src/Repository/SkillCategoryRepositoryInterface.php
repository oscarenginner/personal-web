<?php
/**
 * User: Oscar Sanchez
 * Date: 30/7/20
 */

namespace App\Repository;


use App\Entity\SkillCategory;

interface SkillCategoryRepositoryInterface
{
    public function getAll(): array;

    public function save(SkillCategory $skillCategory) : void;

    public function getById(int $id): SkillCategory;

    public function delete(SkillCategory $skillCategory): void;

    public function findByName($name): ?SkillCategory;

    public function getAllAsKeyValueArray(): array;
}