<?php
/**
 * User: Oscar Sanchez
 * Date: 4/8/20
 */

namespace App\Repository;


use App\Entity\Experience;

interface ExperienceRepositoryInterface
{
    public function getAll(): array;

    public function save(Experience $experience): void;

    public function getAllOrderByDateInit(): array;

    public function getById(int $id) : Experience;

    public function delete(Experience $experience):void;
}