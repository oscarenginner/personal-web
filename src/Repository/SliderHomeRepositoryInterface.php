<?php
/**
 * User: Oscar Sanchez
 * Date: 21/8/20
 */

namespace App\Repository;


use App\Entity\SliderHome;

interface SliderHomeRepositoryInterface
{

    public function getPositionMax() : int;

    public function save(SliderHome $sliderHome);

    public function getAll() : array;

    public function getAllOrderByPosition() : array;

    public function getById(int $id) : SliderHome;

    public function getUpPosition(string $position) : array;

    public function delete(SliderHome $slider) : void;

    public function getEqualsAndUpPositionRangeByTwoPositions(int $position, int $positionTwo) : array;

    public function getDownPositionRangeByTwoPositions(int $position, int $positionTwo) : array;

    public function getLast() : SliderHome;
}