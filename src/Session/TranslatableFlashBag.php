<?php

namespace App\Session;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Contracts\Translation\TranslatorInterface;

class TranslatableFlashBag
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * TranslatableFlashBag constructor.
     *
     * @param Session             $session
     * @param TranslatorInterface $translator
     */
    public function __construct(Session $session, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->translator = $translator;
    }

    private function addTranslatableFlash(string $type, string $message): void
    {
        $flashBag = $this->session->getFlashBag();
        $transMsg = $this->translator->trans($message);

        $flashBag->add($type, $transMsg);
    }

    public function addSuccessFlash(string $message): void
    {
        $this->addTranslatableFlash('success', $message);
    }

    public function addErrorFlash(string $message): void
    {
        $this->addTranslatableFlash('error', $message);
    }

    public function addWarningFlash(string $message): void
    {
        $this->addTranslatableFlash('warning', $message);
    }

    public function addInfoFlash(string $message): void
    {
        $this->addTranslatableFlash('info', $message);
    }
}
