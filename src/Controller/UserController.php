<?php


namespace App\Controller;


use App\Form\Type\UserUpdateType;
use App\Repository\UserRepositoryInterface;
use App\Security\AuthUser;
use App\Service\DTO\CreateUserDTO;
use App\Service\DTO\UserUpdateDTO;
use App\Service\Handler\CreateUserHandler;
use App\Service\Handler\UserUpdateHandler;
use App\Session\TranslatableFlashBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function create(CreateUserHandler $createUserHandler)
    {
        $userCreateDTO = new CreateUserDTO('oscar','oscarsan1986@gmail.com','extremusica');

        $createUserHandler->handle($userCreateDTO);
        return $this->render('security/login.html.twig', [
            'error' => 'hola',
        ]);
    }

    /**
     * @Route("/admin/user/{id}/update", name="user_update")
     * @param Request $request
     * @param UserRepositoryInterface $userRepository
     * @param int $id
     * @param UserUpdateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return Response
     */
    public function update(Request $request,
                           UserRepositoryInterface $userRepository,
                           int $id,
                           UserUpdateHandler $handler,
                           TranslatableFlashBag $flashBag)
    {
        try{
            /** @var AuthUser $authUser */
            $authUser = $this->getUser();
            $user = $authUser->getUser();
            $userUpdate = $userRepository->getById($id);

            if($user->getId() != $user->getId()) {
                throw new NotFoundHttpException();
            }

            $userUpdateDTO = UserUpdateDTO::createByUser($userUpdate);

            $form = $this->createForm( UserUpdateType::class,$userUpdateDTO);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $handler->handle($data);
                $flashBag->addSuccessFlash('user.update.success');

            }
        }catch (UsernameNotFoundException $e) {
            throw new NotFoundHttpException();
        }

        return $this->render('admin/user/update.html.twig',[
            'form' => $form->createView()
        ]);
    }
}