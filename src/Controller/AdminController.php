<?php
/**
 * User: Oscar Sanchez
 * Date: 27/7/20
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{

    /**
     * @Route("/admin/panel", name="admin_panel")
     */

    public function panel()
    {
        return $this->render('admin/admin.base.html.twig', [
        ]);
    }

}