<?php
/**
 * User: Oscar Sanchez
 * Date: 21/8/20
 */

namespace App\Controller;


use App\Exception\SliderHomeNotFoundException;
use App\Form\Type\SliderHomeCreateType;
use App\Form\Type\SliderHomeUpdateType;
use App\Repository\SliderHomeRepositoryInterface;
use App\Service\DTO\SliderHomeChangePositionDTO;
use App\Service\DTO\SliderHomeCreateDTO;
use App\Service\DTO\SliderHomeDeleteDTO;
use App\Service\DTO\SliderHomeUpdateDTO;
use App\Service\Handler\SliderHomeChangePositionHandler;
use App\Service\Handler\SliderHomeCreateHandler;
use App\Service\Handler\SliderHomeDeleteHandler;
use App\Service\Handler\SliderHomeUpdateHandler;
use App\Session\TranslatableFlashBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SliderHomeController extends AbstractController
{

    /**
     * @Route("/admin/slider_home/create", name="slider_home_create")
     * @param Request $request
     * @param SliderHomeCreateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse|Response
     */
    public function create(Request $request , SliderHomeCreateHandler $handler,TranslatableFlashBag $flashBag)
    {
        $sliderCreateDTO = new SliderHomeCreateDTO();
        $form = $this->createForm(SliderHomeCreateType::class,$sliderCreateDTO);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $handler->handle($data);
            $flashBag->addSuccessFlash('slider_home.create.success');

            return $this->redirectToRoute('slider_home_list');

        }

        return $this->render('admin/slider_home/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/slider_home/list", name="slider_home_list")
     * @param SliderHomeRepositoryInterface $sliderHomeRepository
     * @return Response
     */
    public function list(SliderHomeRepositoryInterface $sliderHomeRepository)
    {
        $slidersHome = $sliderHomeRepository->getAllOrderByPosition();

        return $this->render('admin/slider_home/list.html.twig',[
            'sliders' => $slidersHome]);

    }

    /**
     * @Route("/admin/slider_home/{id}/delete", name="slider_home_delete")
     * @param int $id
     * @param SliderHomeDeleteHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse
     */
    public function delete(int $id,SliderHomeDeleteHandler $handler, TranslatableFlashBag $flashBag)
    {
        try {
            $sliderHomeDeleteDTO = new SliderHomeDeleteDTO($id);
            $handler->handle($sliderHomeDeleteDTO);
            $flashBag->addSuccessFlash('slider_home.delete.success');

            return $this->redirectToRoute('slider_home_list');

        }catch (SliderHomeNotFoundException $e) {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/ajax/slider_home/change_position/", name="slider_home_change_position")
     * @param Request $request
     * @param SliderHomeChangePositionHandler $handler
     * @return JsonResponse|RedirectResponse
     */
    public function ajaxChangePosition(Request $request, SliderHomeChangePositionHandler $handler)
    {
        if (!$request->isXmlHttpRequest()) {
            return $this->redirectToRoute('index');
        }

        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);

        $sliderId = $request->query->get('id');

        $targetPosition = $request->get('targetPosition');

        if(!$sliderId || !$targetPosition) {

            $response = $serializer->serialize([
                'result' => 'error'
            ], 'json');

            return new JsonResponse($response, Response::HTTP_BAD_REQUEST, [], true);

        }

        $sliderHomeChangePositionDTO = new SliderHomeChangePositionDTO($sliderId,$targetPosition);

        $handler->handle($sliderHomeChangePositionDTO);

        $response = $serializer->serialize([
            'result' => 'success'
        ], 'json');

        return new JsonResponse($response, Response::HTTP_OK, [] , true);

    }

    /**
     * @Route("/admin/slider_home/{id}/update", name="slider_home_update")
     * @param Request $request
     * @param int $id
     * @param SliderHomeRepositoryInterface $sliderHomeRepository
     * @param SliderHomeUpdateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse|Response
     */
    public function update(Request $request,
                           int $id,
                           SliderHomeRepositoryInterface $sliderHomeRepository,
                           SliderHomeUpdateHandler $handler,
                           TranslatableFlashBag $flashBag)
    {
        try {
            $sliderHome = $sliderHomeRepository->getById($id);
            $sliderHomeUpdateDTO = SliderHomeUpdateDTO::createBySliderHome($sliderHome);
            $form = $this->createForm(SliderHomeUpdateType::class,$sliderHomeUpdateDTO);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();

                $handler->handle($data);
                $flashBag->addSuccessFlash('slider_image.update.success');

                return $this->redirectToRoute('slider_home_list');

            }

            return $this->render('admin/slider_home/update.html.twig', [
                'form' => $form->createView(),
                'image' => $sliderHome->getImage(),
                'imagePhone' => $sliderHome->getImagePhone()
            ]);



        }catch (SliderHomeNotFoundException $e) {
            throw new NotFoundHttpException();
        }
    }

}