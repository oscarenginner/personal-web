<?php
/**
 * User: Oscar Sanchez
 * Date: 24/8/20
 */

namespace App\Controller;


use App\Repository\PostRepositoryInterface;
use App\Repository\ProjectRepositoryInterface;
use App\Repository\SkillRepositoryInterface;
use App\Repository\SliderHomeRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param SliderHomeRepositoryInterface $sliderHomeRepository
     * @param UserRepositoryInterface $userRepository
     * @param SkillRepositoryInterface $skillRepository
     * @param PostRepositoryInterface $postRepository
     * @param ProjectRepositoryInterface $projectRepository
     * @return Response
     */
    public function index(SliderHomeRepositoryInterface $sliderHomeRepository,
                          UserRepositoryInterface $userRepository,
                          SkillRepositoryInterface $skillRepository,
                          PostRepositoryInterface $postRepository,
                          ProjectRepositoryInterface $projectRepository)
    {
        $slider = $sliderHomeRepository->getLast();

        $user = $userRepository->getById(1);

        $skillsFeatured = $skillRepository->getAllFeatured();

        $posts = $postRepository->getLastPublish(3);

        $projects = $projectRepository->getFeatured();


        return $this->render('pages/home.html.twig',[
            'slider' => $slider,
            'user' => $user,
            'skillsFeatured' => $skillsFeatured,
            'posts'  => $posts,
            'projects' => $projects
        ]);
    }
}