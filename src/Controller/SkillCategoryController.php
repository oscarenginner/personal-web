<?php
/**
 * User: Oscar Sanchez
 * Date: 30/7/20
 */

namespace App\Controller;


use App\Exception\SkillCategoryNotFoundException;
use App\Form\Type\SkillCategoryCreateType;
use App\Form\Type\SkillCategoryUpdateType;
use App\Repository\SkillCategoryRepositoryInterface;
use App\Service\DTO\SkIllCategoryDeleteDTO;
use App\Service\DTO\SkillCategoryUpdateDTO;
use App\Service\Handler\SkillCategoryCreateHandler;
use App\Service\Handler\SkillCategoryDeleteHandler;
use App\Service\Handler\SkillCategoryUpdateHandler;
use App\Session\TranslatableFlashBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class SkillCategoryController extends AbstractController
{
    /**
     * @Route("/admin/skill-category/create", name="skill_category_create")
     * @param Request $request
     * @param SkillCategoryCreateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return Response
     */
    public function create(Request $request, SkillCategoryCreateHandler $handler, TranslatableFlashBag $flashBag)
    {
        $form = $this->createForm(SkillCategoryCreateType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $handler->handle($data);
            $flashBag->addSuccessFlash('skill.category.create.success');

            return $this->redirectToRoute('skill_category_list');
        }

        return $this->render('admin/skill/category/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/skill-category/list", name="skill_category_list")
     * @param SkillCategoryRepositoryInterface $skillCategoryRepository
     * @return Response
     */
    public function list(SkillCategoryRepositoryInterface $skillCategoryRepository)
    {
        $skillCategories = $skillCategoryRepository->getAll();

        return $this->render('admin/skill/category/list.html.twig', [
            'skillCategories' => $skillCategories
        ]);

    }

    /**
     * @Route("admin/skill-category/{id}/delete", name="skill_category_delete")
     * @param int $id
     * @param SkillCategoryDeleteHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse
     */
    public function delete(int $id, SkillCategoryDeleteHandler $handler, TranslatableFlashBag $flashBag)
    {
        try {
            $skillCategoryDeleteDTO = new SkIllCategoryDeleteDTO($id);
            $handler->handle($skillCategoryDeleteDTO);
            $flashBag->addSuccessFlash('skill.category.delete.success');
            return $this->redirectToRoute('skill_category_list');

        }catch (SkillCategoryNotFoundException $e) {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("admin/skill-category/{id}/update", name="skill_category_update")
     * @param Request $request
     * @param int $id
     * @param SkillCategoryRepositoryInterface $skillCategoryRepository
     * @param SkillCategoryUpdateHandler $handler
     * @param TranslatableFlashBag $flashBag
     */
    public function update(Request $request,
                           int $id,
                           SkillCategoryRepositoryInterface $skillCategoryRepository,
                            SkillCategoryUpdateHandler $handler,
                           TranslatableFlashBag $flashBag)
    {
        try {
            $skillCategory = $skillCategoryRepository->getById($id);
            $skillCategoryUpdateDTO = new SkillCategoryUpdateDTO($skillCategory->getId(),$skillCategory->getName());
            $form = $this->createForm(SkillCategoryUpdateType::class,$skillCategoryUpdateDTO);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $handler->handle($data);
                $flashBag->addSuccessFlash('skill.category.update.success');
                return $this->redirectToRoute('skill_category_list');

            }

            return $this->render('admin/skill/category/update.html.twig', [
                'form' => $form->createView(),
                'icon' => $skillCategory->getIcon()
            ]);

        }catch (SkillCategoryNotFoundException $e) {
            throw new NotFoundHttpException();
        }
    }

}