<?php
/**
 * User: Oscar Sanchez
 * Date: 4/8/20
 */

namespace App\Controller;


use App\Exception\ExperienceDataIsNotCorrectException;
use App\Exception\ExperienceNotFoundException;
use App\Exception\UserNotIsPropertyOfObjectException;
use App\Form\Type\ExperienceCreateType;
use App\Form\Type\ExperienceUpdateType;
use App\Repository\ExperienceRepositoryInterface;
use App\Security\AuthUser;
use App\Service\DTO\ExperienceCreateDTO;
use App\Service\DTO\ExperienceDeleteDTO;
use App\Service\DTO\ExperienceUpdateDTO;
use App\Service\Handler\ExperienceCreateHandler;
use App\Service\Handler\ExperienceDeleteHandler;
use App\Service\Handler\ExperienceUpdateHandler;
use App\Session\TranslatableFlashBag;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ExperienceController extends AbstractController
{

    /**
     * @Route("/admin/experience/create", name="experience_create")
     * @param Request $request
     * @param ExperienceCreateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return Response
     * @throws ExperienceDataIsNotCorrectException
     */
    public function create(Request $request, ExperienceCreateHandler $handler, TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        $experienceCreateDTO = new ExperienceCreateDTO($user->getId());
        $form = $this->createForm(ExperienceCreateType::class,$experienceCreateDTO);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $handler->handle($data);
            $flashBag->addSuccessFlash('experience.create.success');
            return $this->redirectToRoute('experience_list');

        }

        return $this->render('admin/experience/create.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/admin/experience/list", name="experience_list")
     * @param ExperienceRepositoryInterface $experienceRepository
     * @return Response
     */
    public function list(ExperienceRepositoryInterface $experienceRepository)
    {
        $experiences = $experienceRepository->getAllOrderByDateInit();

        return $this->render('admin/experience/list.html.twig', [
            'experiences' => $experiences
        ]);

    }

    /**
     * @Route("/admin/experience/{id}/delete", name="experience_delete")
     * @param int $id
     * @param ExperienceDeleteHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse
     */
    public function delete(int $id, ExperienceDeleteHandler $handler, TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        try {
            $experienceDeleteDTO = new ExperienceDeleteDTO($id,$user->getId());
            $handler->handle($experienceDeleteDTO);
            $flashBag->addSuccessFlash('experience.delete.success');
            return $this->redirectToRoute('experience_list');

        }catch (ExperienceNotFoundException $e) {
            throw new NotFoundHttpException();
        }catch (UserNotIsPropertyOfObjectException $e) {

            throw new AccessDeniedHttpException();
        }
    }


    /**
     * @Route("/admin/experience/{id}/update", name="experience_update")
     * @param Request $request
     * @param int $id
     * @param ExperienceRepositoryInterface $experienceRepository
     * @param ExperienceUpdateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @throws Exception
     */
    public function update(Request $request,
                           int $id,
                           ExperienceRepositoryInterface $experienceRepository,
                           ExperienceUpdateHandler $handler,
                           TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        try {
            $experience = $experienceRepository->getById($id);
            $experienceUpdateDTO = ExperienceUpdateDTO::createByExperience($user->getId(),$experience);
            $form = $this->createForm(ExperienceUpdateType::class,$experienceUpdateDTO);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $handler->handle($data);
                $flashBag->addSuccessFlash('update.experience.success');
                return $this->redirectToRoute('experience_list');
            }

            return $this->render('admin/experience/update.html.twig', [
                'form' => $form->createView(),
                'experienceImage' => $experience->getImage()
            ]);

        }catch (ExperienceNotFoundException $e){
            throw new NotFoundHttpException();
        }catch (UserNotIsPropertyOfObjectException $e) {
            throw new AccessDeniedHttpException();
        }

    }

}