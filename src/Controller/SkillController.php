<?php
/**
 * User: Oscar Sanchez
 * Date: 3/8/20
 */

namespace App\Controller;

use App\Exception\UserNotIsPropertyOfObjectException;
use App\Form\Type\SkillCreateType;
use App\Form\Type\SkillUpdateType;
use App\Repository\SkillRepositoryInterface;
use App\Security\AuthUser;
use App\Service\DTO\SkillCreateDTO;
use App\Service\DTO\SkillDeleteDTO;
use App\Service\DTO\SkillUpdateDTO;
use App\Service\Handler\SkillCreateHandler;
use App\Service\Handler\SkillDeleteHandler;
use App\Service\Handler\SkillUpdateHandler;
use App\Session\TranslatableFlashBag;
use App\Exception\SkillNotFoundException;
use App\Exception\SkillScoredNotInRangeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class SkillController extends AbstractController
{
    /**
     * @Route("/admin/skill/create", name="skill_create")
     * @param Request $request
     * @param SkillCreateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return Response
     * @throws SkillScoredNotInRangeException
     */
    public function create(Request $request, SkillCreateHandler $handler, TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        $skillCreateDTO = new SkillCreateDTO($user->getId());
        $form = $this->createForm(SkillCreateType::class,$skillCreateDTO);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $handler->handle($data);
            $flashBag->addSuccessFlash('skill.create.success');
            return $this->redirectToRoute('skill_list');
        }

        return $this->render('admin/skill/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/skill/list", name="skill_list")
     * @param SkillRepositoryInterface $skillRepository
     */
    public function list(SkillRepositoryInterface $skillRepository)
    {
        $skills = $skillRepository->getAll();

        return $this->render('admin/skill/list.html.twig', [
            'skills' => $skills
        ]);
    }

    /**
     * @Route("/admin/skill/{id}/delete", name="skill_delete")
     * @param int $id
     * @param SkillDeleteHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse
     */
    public function delete(int $id,SkillDeleteHandler $handler, TranslatableFlashBag $flashBag)
    {
        /**ToDO: HACER QUE SOLO PUEDA ELIMINARLA EL USUARIO**/
        try {
            $skillDeleteDTO = new SkillDeleteDTO($id);
            $handler->handle($skillDeleteDTO);
            $flashBag->addSuccessFlash('skill.delete.success');
            return $this->redirectToRoute('skill_list');


        }catch (SkillNotFoundException $e) {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/admin/skill/{id}/update", name="skill_update")
     * @param Request $request
     * @param int $id
     * @param SkillRepositoryInterface $skillRepository
     * @param SkillUpdateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @throws SkillScoredNotInRangeException
     */
    public function update(Request $request,
                           int $id,
                           SkillRepositoryInterface $skillRepository,
                           SkillUpdateHandler $handler,
                           TranslatableFlashBag $flashBag)
    {
        try {

            $skill = $skillRepository->getById($id);
            $skillUpdateDTO = SkillUpdateDTO::createFormSkill($skill);
            $form = $this->createForm(SkillUpdateType::class,$skillUpdateDTO);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $handler->handle($data);
                $flashBag->addSuccessFlash('skill.update.success');
                return $this->redirectToRoute('skill_list');
            }

            return $this->render('admin/skill/update.html.twig', [
                'form' => $form->createView(),
                'image' => $skill->getImage()
            ]);


        }catch (SkillNotFoundException $e) {
            throw new NotFoundHttpException();
        }catch (UserNotIsPropertyOfObjectException $e) {
            throw new AccessDeniedHttpException();
        }

    }

}