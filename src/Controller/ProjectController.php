<?php
/**
 * User: Oscar Sanchez
 * Date: 5/8/20
 */

namespace App\Controller;


use App\Exception\ProjectNotFoundException;
use App\Exception\UserNotIsPropertyOfObjectException;
use App\Form\Type\ProjectCreateType;
use App\Form\Type\ProjectUpdateType;
use App\Repository\ProjectRepositoryInterface;
use App\Security\AuthUser;
use App\Service\DTO\ProjectCreateDTO;
use App\Service\DTO\ProjectDeleteDTO;
use App\Service\DTO\ProjectImageDeleteDTO;
use App\Service\DTO\ProjectUpdateDTO;
use App\Service\Handler\ProjectCreateHandler;
use App\Service\Handler\ProjectDeleteHandler;
use App\Service\Handler\ProjectImageDeleteHandler;
use App\Service\Handler\ProjectUpdateHandler;
use App\Session\TranslatableFlashBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ProjectController extends AbstractController
{
    /**
     * @Route("/admin/project/create", name="project_create")
     * @param Request $request
     * @param ProjectCreateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return Response
     */
    public function create(Request $request,
                           ProjectCreateHandler $handler,
                           TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        $projectCreateDTO = new ProjectCreateDTO($user->getId());
        $form = $this->createForm(ProjectCreateType::class,$projectCreateDTO);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $handler->handle($data);
            $flashBag->addSuccessFlash('project.create.success');
            return $this->redirectToRoute('project_list');
        }

        return $this->render('admin/project/create.html.twig',[
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/admin/project/list", name="project_list")
     * @param ProjectRepositoryInterface $projectRepository
     * @return Response
     */
    public function list(ProjectRepositoryInterface $projectRepository)
    {
        $projects = $projectRepository->getAllOrderByDate();

        return $this->render('admin/project/list.html.twig', [
            'projects' => $projects
        ]);
    }

    /**
     * @Route("/admin/project/{id}/delete", name="project_delete")
     * @param int $id
     * @param ProjectDeleteHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse
     */
    public function delete(int $id, ProjectDeleteHandler $handler, TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        try {
            $projectDeleteDTO = new ProjectDeleteDTO($id, $user->getId());
            $handler->handle($projectDeleteDTO);
            $flashBag->addSuccessFlash('project.delete.success');
            return $this->redirectToRoute('project_list');

        }catch (ProjectNotFoundException $e) {
            throw new  NotFoundHttpException();

        }catch (UserNotIsPropertyOfObjectException $e) {
            throw new AccessDeniedHttpException();
        }

    }

    /**
     * @Route("/admin/project/{id}/update", name="project_update")
     * @param Request $request
     * @param int $id
     * @param ProjectRepositoryInterface $projectRepository
     * @param ProjectUpdateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse|Response
     */
    public function update(Request $request,
                           int $id,
                           ProjectRepositoryInterface $projectRepository ,
                           ProjectUpdateHandler $handler,
                           TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        try {
            $project = $projectRepository->getById($id);
            $projectUpdateDTO = ProjectUpdateDTO::createByProject($project,$user->getId());
            $form = $this->createForm(ProjectUpdateType::class,$projectUpdateDTO);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $handler->handle($data);
                $flashBag->addSuccessFlash('project.update.success');
                return $this->redirectToRoute('project_list');
            }

            return $this->render('admin/project/update.html.twig', [
                'form' => $form->createView(),
                'logo' => $project->getLogo(),
                'imageFull' => $project->getImageFull(),
                'imageList' => $project->getImageList(),
                'imageListPhone' => $project->getImageListPhone(),
                'file' => $project->getFile(),
                'buttonText' => $project->getButtonFileText(),
                'images' => $project->getImages()
            ]);

        }catch (ProjectNotFoundException $e) {
            throw new NotFoundHttpException();

        }catch (UserNotIsPropertyOfObjectException $e) {
            throw new AccessDeniedHttpException();
        }

    }

    /**
     * @Route("/ajax/image/project/remove", name="remove_project_image_ajax")
     * @param Request $request
     * @param ProjectImageDeleteHandler $handler
     * @return RedirectResponse
     */
    public function deleteImage(Request $request, ProjectImageDeleteHandler $handler)
    {

        if (!$request->isXmlHttpRequest()) {
            return $this->redirectToRoute('index');
        }

        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);

        $imageID = $request->query->get('image');

        if(empty($imageID)) {

            $response = $serializer->serialize([
                'result' => 'error'
            ], 'json');

            return new JsonResponse($response, Response::HTTP_BAD_REQUEST, [], true);
        }

        $projectImageDeleteDTO  = new ProjectImageDeleteDTO($imageID);
        $handler->handle($projectImageDeleteDTO);

        $response = $serializer->serialize([
            'result' => 'success'
        ], 'json');

        return new JsonResponse($response, Response::HTTP_OK, [], true);

    }


}