<?php
/**
 * User: Oscar Sanchez
 * Date: 19/8/20
 */

namespace App\Controller;


use App\Exception\BlogCategoryExceptionNotFound;
use App\Exception\PostNotFoundException;
use App\Exception\UserNotIsPropertyOfObjectException;
use App\Form\Type\BlogCategoryCreateType;
use App\Form\Type\BlogCategoryUpdateType;
use App\Form\Type\PostCreateType;
use App\Form\Type\PostUpdateType;
use App\Repository\BlogCategoryRepositoryInterface;
use App\Repository\PostRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use App\Security\AuthUser;
use App\Service\DTO\BlogCategoryCreateDTO;
use App\Service\DTO\BlogCategoryDeleteDTO;
use App\Service\DTO\BlogCategoryUpdateDTO;
use App\Service\DTO\BlogQueryDTO;
use App\Service\DTO\PostCreateDTO;
use App\Service\DTO\PostDeleteDTO;
use App\Service\DTO\PostUpdateDTO;
use App\Service\Handler\BlogCategoryCreateHandler;
use App\Service\Handler\BlogCategoryDeleteHandler;
use App\Service\Handler\BlogCategoryUpdateHandler;
use App\Service\Handler\PostCreateHandler;
use App\Service\Handler\PostDeleteHandler;
use App\Service\Handler\PostUpdateHandler;
use App\Service\Query\BlogQuery;
use App\Session\TranslatableFlashBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{

    /**
     * @Route("/admin/blog/category/create", name="blog_category_create")
     * @param Request $request
     * @param BlogCategoryCreateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse|Response
     */
    public function categoryCreate(Request $request, BlogCategoryCreateHandler $handler, TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        $blogCategoryCreateDTO = new BlogCategoryCreateDTO($user->getId());
        $form = $this->createForm(BlogCategoryCreateType::class,$blogCategoryCreateDTO);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $handler->handle($data);
            $flashBag->addSuccessFlash('blog_category.create.success');

            return $this->redirectToRoute('blog_category_list');
        }

        return $this->render('admin/blog/category/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/blog/category/list", name="blog_category_list")
     * @param BlogCategoryRepositoryInterface $blogCategoryRepository
     * @return Response
     */
    public function categoryList(BlogCategoryRepositoryInterface $blogCategoryRepository)
    {
        $blogCategories = $blogCategoryRepository->getAll();

        return $this->render('admin/blog/category/list.html.twig', [
            'blogCategories' => $blogCategories
        ]);

    }

    /**
     * @Route("/admin/blog/category/{id}/delete", name="blog_category_delete")
     * @param int $id
     * @param BlogCategoryDeleteHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse
     */
    public function categoryDelete(int $id, BlogCategoryDeleteHandler $handler, TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        try {
            $blogCategoryDeleteDTO = new BlogCategoryDeleteDTO($user->getId(),$id);
            $handler->handle($blogCategoryDeleteDTO);
            $flashBag->addSuccessFlash('blog.category.delete.success');

            return $this->redirectToRoute('blog_category_list');

        }catch (BlogCategoryExceptionNotFound $e) {
            throw new NotFoundHttpException();
        }catch (UserNotIsPropertyOfObjectException $e) {
            throw new AccessDeniedHttpException();
        }

    }

    /**
     * @Route("/admin/blog/category/{id}/update", name="blog_category_update")
     * @param Request $request
     * @param int $id
     * @param BlogCategoryRepositoryInterface $blogCategoryRepository
     * @param BlogCategoryUpdateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return Response
     */
    public function categoryUpdate(Request $request,
                                   int $id,
                                   BlogCategoryRepositoryInterface $blogCategoryRepository,
                                   BlogCategoryUpdateHandler $handler,
                                   TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        try {
            $blogCategory = $blogCategoryRepository->getById($id);
            $blogCategoryUpdateDTO = BlogCategoryUpdateDTO::createByCategoryAndUserId($blogCategory,$user->getId());
            $form = $this->createForm(BlogCategoryUpdateType::class,$blogCategoryUpdateDTO);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $handler->handle($data);
                $flashBag->addSuccessFlash('blog.category.success.update');
                return $this->redirectToRoute('blog_category_list');
            }

            return $this->render('admin/blog/category/update.html.twig', [
                'form' => $form->createView(),
                'blogCategoryImage' => $blogCategory->getImage()
            ]);


        }catch (BlogCategoryExceptionNotFound $e) {
            throw new NotFoundHttpException();
        }catch (UserNotIsPropertyOfObjectException $e) {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * @Route("/admin/blog/post/create", name="blog_post_create")
     * @param Request $request
     * @param PostCreateHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse|Response
     */
    public function postCreate(Request $request, PostCreateHandler $handler, TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        $postCreateDTO = new PostCreateDTO($user->getId());
        $form = $this->createForm(PostCreateType::class,$postCreateDTO);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $handler->handle($postCreateDTO);
            $flashBag->addSuccessFlash('post.create.success');

            return $this->redirectToRoute('blog_post_create');

        }

        return $this->render('admin/blog/post/create.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/admin/blog/post/list", name="blog_post_list")
     * @param PostRepositoryInterface $postRepository
     * @return Response
     */
    public function postList(PostRepositoryInterface $postRepository)
    {
        $posts = $postRepository->getAll();

        return $this->render('admin/blog/post/list.html.twig', [
            'posts' => $posts
        ]);


    }

    /**
     * @Route("/admin/blog/post/{id}/delete", name="blog_post_delete")
     * @param int $id
     * @param PostDeleteHandler $handler
     * @param TranslatableFlashBag $flashBag
     * @return RedirectResponse
     */
    public function postDelete(int $id, PostDeleteHandler $handler, TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        try {
            $postDeleteDTO = new PostDeleteDTO($id,$user->getId());
            $handler->handle($postDeleteDTO);
            $flashBag->addSuccessFlash('post.delete.success');
            return $this->redirectToRoute('blog_post_list');


        }catch (PostNotFoundException $e) {
            throw new NotFoundHttpException();
        }catch (UserNotIsPropertyOfObjectException $e) {
            throw new AccessDeniedHttpException();
        }

    }

    /**
     * @Route("/admin/blog/post/{id}/update", name="blog_post_update")
     * @param Request $request
     * @param int $id
     * @param PostRepositoryInterface $postRepository
     * @param PostUpdateHandler $handler
     * @param TranslatableFlashBag $flashBag
     */
    public function postUpdate(Request $request,
                               int $id ,
                               PostRepositoryInterface $postRepository,
                               PostUpdateHandler $handler,
                               TranslatableFlashBag $flashBag)
    {
        /**@var AuthUser $userApp */
        $userApp = $this->getUser();
        $user = $userApp->getUser();

        try {
            $post = $postRepository->getById($id);
            $postUpdateDTO = PostUpdateDTO::createByPostAndUserId($post,$user->getId());
            $form = $this->createForm(PostUpdateType::class,$postUpdateDTO);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $handler->handle($data);
                $flashBag->addSuccessFlash('post.update.success');
                return $this->redirectToRoute('blog_post_list');

            }

        }catch (PostNotFoundException $e) {
            throw new NotFoundHttpException();
        }

        return $this->render('admin/blog/post/update.html.twig', [
            'form' => $form->createView(),
            'imageList' => $post->getImageList(),
            'imageFull' => $post->getImage()
        ]);
    }

    /**
     * @Route("/blog", name="blog_page")
     * @param Request $request
     * @param BlogQuery $blogQuery
     * @param UserRepositoryInterface $userRepository
     * @param PostRepositoryInterface $postRepository
     * @param BlogCategoryRepositoryInterface $categoryRepository
     * @return Response
     */
    public function list(Request $request,
                         BlogQuery $blogQuery ,
                         UserRepositoryInterface $userRepository,
                         PostRepositoryInterface $postRepository,
                         BlogCategoryRepositoryInterface $categoryRepository)
    {
        $query = $request->query->all();

        $query = BlogQueryDTO::createFromQuery($query);

        $blogQuery->query($query);

        $queryResult = $blogQuery->query($query);
        $user = $userRepository->getById(1);
        $lastPost = $postRepository->getLastPublish(3);
        $categoriesIdWithPost = $postRepository->getDistinctCategories();
        $categoriesWithPost = [];

        if($categoriesIdWithPost) {
            foreach ($categoriesIdWithPost as $categoryId) {
                $categoriesWithPost[] = $categoryRepository->getById($categoryId[1]);
            }
        }

        return $this->render('blog/list.html.twig', [
            'user' => $user,
            'query' => $queryResult,
            'lastPosts' => $lastPost,
            'categoryWithPost' => $categoriesWithPost
        ]);

    }


    /**
     * @Route("/blog/category/{id}/{slug}", name="blog_category_page")
     * @param Request $request
     * @param BlogQuery $blogQuery
     * @param int $id
     * @param UserRepositoryInterface $userRepository
     * @param PostRepositoryInterface $postRepository
     * @param BlogCategoryRepositoryInterface $categoryRepository
     * @return Response
     */
    public function listCategory(Request $request,
                         BlogQuery $blogQuery ,
                         int $id,
                         UserRepositoryInterface $userRepository,
                         PostRepositoryInterface $postRepository,
                         BlogCategoryRepositoryInterface $categoryRepository)
    {
        $query = $request->query->all();

        $query = BlogQueryDTO::createFromQuery($query);

        $blogQuery->query($query);

        $queryResult = $blogQuery->query($query);
        $user = $userRepository->getById(1);
        $lastPost = $postRepository->getLastPublish(3);
        $categoriesIdWithPost = $postRepository->getDistinctCategories();
        $category = $categoryRepository->getById($id);
        $categoriesWithPost = [];

        if($categoriesIdWithPost) {
            foreach ($categoriesIdWithPost as $categoryId) {
                $categoriesWithPost[] = $categoryRepository->getById($categoryId[1]);
            }
        }

        return $this->render('blog/category-list.html.twig', [
            'user' => $user,
            'query' => $queryResult,
            'lastPosts' => $lastPost,
            'categoryWithPost' => $categoriesWithPost,
            'category' => $category
        ]);

    }




}